/**
 ******************************************************************************
 **	t@Cź : balancer_private.h
 **
 ** fÖAîń:
 **   fź   : balancer.mdl
 **   o[W : 1.893
 **   đ       : y_yama - Tue Sep 25 11:37:09 2007
 **                takashic - Sun Sep 28 17:50:53 2008
 **
 ** Copyright (c) 2009-2016 MathWorks, Inc.
 ** All rights reserved.
 ******************************************************************************
 **/

/* Imported (extern) block parameters */
extern float A_D;                   /* Variable: A_D
                                        * Referenced by blocks:
                                        * '<S11>/Constant1'
                                        * '<S11>/Gain2'
                                        * [pXtB^W(śEÔÖĚ˝Ďń]pxp)
                                        */
extern float A_R;                   /* Variable: A_R
                                        * Referenced by blocks:
                                        * '<S8>/Constant1'
                                        * '<S8>/Gain2'
                                        * [pXtB^W(śEÔÖĚÚW˝Ďń]pxp)
                                        */
extern float K_F[4];                /* Variable: K_F
                                        * '<S1>/FeedbackGain'
                                        * T[{§äpóÔtB[hobNW
                                        */
extern float K_I;                   /* Variable: K_I
                                        * '<S1>/IntegralGain'
                                        * T[{§äpĎŞW
                                        */
extern float K_PHIDOT;              /* Variable: K_PHIDOT
                                        * '<S3>/Gain2'
                                        * ÔĚĚÚW˝Ęń]Źx(dÓ/dt)W
                                        */
extern float K_THETADOT;            /* Variable: K_THETADOT
                                        * '<S3>/Gain1'
                                        * śEÔÖĚ˝Ďń]Źx(dĆ/dt)W
                                        */
extern const float BATTERY_GAIN;    /* PWMoÍZopobedłâłW */
extern const float BATTERY_OFFSET;  /* PWMoÍZopobedłâłItZbg */

/*======================== TOOL VERSION INFORMATION ==========================*
 * MATLAB 7.7 (R2008b)30-Jun-2008                                             *
 * Simulink 7.2 (R2008b)30-Jun-2008                                           *
 * Real-Time Workshop 7.2 (R2008b)30-Jun-2008                                 *
 * Real-Time Workshop Embedded Coder 5.2 (R2008b)30-Jun-2008                  *
 * Stateflow 7.2 (R2008b)30-Jun-2008                                          *
 * Stateflow Coder 7.2 (R2008b)30-Jun-2008                                    *
 * Simulink Fixed Point 6.0 (R2008b)30-Jun-2008                               *
 *============================================================================*/

/*======================= LICENSE IN USE INFORMATION =========================*
 * matlab                                                                     *
 * real-time_workshop                                                         *
 * rtw_embedded_coder                                                         *
 * simulink                                                                   *
 *============================================================================*/
/******************************** END OF FILE ********************************/

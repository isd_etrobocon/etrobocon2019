/**
 * @file Log.h
 * @brief ログ出力
 * @author 平井 景
 * @date 2019/06/26
 */

#ifndef __LOG_H__
#define __LOG_H__

#include <ev3api.h>
#include "System/Bluetooth.h"

//! [デバッグログ出力]
// ファイル名、行、関数名を出力。
// コミットコードに残しても良いが、
// 毎周期出力されるような場所には設置しないこと！！
#define DEBUG_LOG \
{\
	char tmp[100]={};\
	snprintf(tmp,sizeof(tmp)-1,"[DEBUG_LOG] %s(%d)\n",__PRETTY_FUNCTION__,__LINE__);\
	Bluetooth::getInstance().send(tmp);\
	Log::getInstance().storedLog(tmp);\
}

//! [デバッグログ出力（引数付き）]
// ファイル名、行、関数名を出力。
// 引数には文字列を指定（改行不要）。
// コミットコードに残しても良いが、
// 毎周期出力されるような場所には設置しないこと！！
#define DEBUG_LOGS(log) \
{\
	char tmp[100]={};\
	snprintf(tmp,sizeof(tmp)-1,"[DEBUG_LOG] %s(%d) : %s\n",__PRETTY_FUNCTION__,__LINE__,log);\
	Bluetooth::getInstance().send(tmp);\
	Log::getInstance().storedLog(tmp);\
}

//! [デバッグログ出力（フォーマット指定子）]
// ファイル名、行、関数名を出力。
// 引数にはフォーマット指定子付きの文字列を指定（改行不要）。
// コミットコードに残しても良いが、
// 毎周期出力されるような場所には設置しないこと！！
#define DEBUG_LOGF(fmt, ...) \
{\
	char tmp[64]={};\
	snprintf(tmp,sizeof(tmp)-1,fmt,__VA_ARGS__);\
	char tmp2[100]={};\
	snprintf(tmp2,sizeof(tmp2)-1,"[DEBUG_LOG] %s(%d) : %s\n",__PRETTY_FUNCTION__,__LINE__,tmp);\
	Bluetooth::getInstance().send(tmp2);\
	Log::getInstance().storedLog(tmp);\
}

//! [蓄積ログ保存]
// ファイル名、行、関数名を出力。
// リアルタイムログとは違い、内部バッファにログを蓄積する（最大10000件）
// Bluetoothからのログ出力指示で蓄積ログを一括送信する。
// このログは毎周期出力されるような場所への設置＆コミットもOK。
#define STORED_LOG \
{\
	char tmp[100]={};\
	snprintf(tmp,sizeof(tmp)-1,"[STORED_LOG] %s(%d)\n",__PRETTY_FUNCTION__,__LINE__);\
	Log::getInstance().storedLog(tmp);\
}

//! [蓄積ログ保存（引数付き）]
// ファイル名、行、関数名を出力。
// 引数には文字列を指定（改行不要）。
// リアルタイムログとは違い、内部バッファにログを蓄積する（最大10000件）
// Bluetoothからのログ出力指示で蓄積ログを一括送信する。
// このログは毎周期出力されるような場所への設置＆コミットもOK。
#define STORED_LOGS(log) \
{\
	char tmp[100]={};\
	snprintf(tmp,sizeof(tmp)-1,"[STORED_LOG] %s(%d) : %s\n",__PRETTY_FUNCTION__,__LINE__,log);\
	Log::getInstance().storedLog(tmp);\
}

//! [蓄積ログ保存（フォーマット指定子）]
// ファイル名、行、関数名を出力。
// 引数にはフォーマット指定子付きの文字列を指定（改行不要）。
// リアルタイムログとは違い、内部バッファにログを蓄積する（最大10000件）
// Bluetoothからのログ出力指示で蓄積ログを一括送信する。
// このログは毎周期出力されるような場所への設置＆コミットもOK。
#define STORED_LOGF(fmt, ...) \
{\
	char tmp[64]={};\
	snprintf(tmp,sizeof(tmp)-1,fmt,__VA_ARGS__);\
	char tmp2[100]={};\
	snprintf(tmp2,sizeof(tmp2)-1,"[STORED_LOG] %s(%d) : %s\n",__PRETTY_FUNCTION__,__LINE__,tmp);\
	Log::getInstance().storedLog(tmp2);\
}

//! [デバッグ用（開発時の確認用）]
// コードに残った状態でコミットしないこと！！
#define CHECKPOINT \
{\
	char tmp[128]={};\
	snprintf(tmp,sizeof(tmp)-1,"[CHECKPOINT] %s(%d) %s\n",__FILE__,__LINE__,__PRETTY_FUNCTION__);\
	Bluetooth::getInstance().send(tmp);\
}


static const int LOG_ARRAY_MAX_NUM = 10000;		//! 蓄積ログ最大行数
static const int LOG_ARRAY_MAX_LEN = 100;		//! 蓄積ログ最大文字数

/**
 * @brief Log出力クラス
 * @author 北村 衿子
 * @date 2019/08/14
 */

class Log
{
public:
	/**
	 * @brief シングルトンなインスタンスを取得
	 * @author 北村 衿子
	 * @date 2019/08/14
	 * @return Logクラスインスタンス
	 */
	static Log& getInstance();

	/**
	 * @brief ログデータを蓄積
	 * @author 北村 衿子
	 * @date 2019/08/14
	 * @param [in] data 蓄積用ログデータ
	 */
	void storedLog( char* data );

	/**
	 * @brief 蓄積ログデータを出力
	 * @author 北村 衿子
	 * @date 2019/08/15
	 */
	void printStoredLogs();

private:
	/**
	 * @brief コンストラクタ
	 * @author 北村 衿子
	 * @date 2019/08/14
	 */
	Log();

	/**
	 * @brief デストラクタ
	 * @author 北村 衿子
	 * @date 2019/08/14
	 */
	~Log();

	/**
	 * @brief コピーコンストラクタ
	 * @author 北村 衿子
	 * @date 2019/08/15
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Log( const Log& );

	char m_storedLogs[ LOG_ARRAY_MAX_NUM ][ LOG_ARRAY_MAX_LEN ];		//! ログ蓄積用配列	
	int m_index;														//! 配列インデックス
	bool m_isMax;														//! 1万ログ以上判定
};
#endif // __LOG_H__

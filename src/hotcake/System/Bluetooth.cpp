/**
 * @file Bluetooth.cpp
 * @brief Bluetooth通信
 * @author 平井 景
 * @date 2019/06/26
 */
#include <ev3api.h>
#include "Bluetooth.h"
#include "Config.h"

/**
 * @brief Bluetooth受信タスク
 * @author 平井 景
 * @date 2019/06/26
 * @param [in] exinf 未使用
 */
void btTask(intptr_t exinf)
{
	Bluetooth& scenario = Bluetooth::getInstance();
	scenario.receiveLoop();
	ext_tsk();
}

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
Bluetooth::Bluetooth()
{
	m_serialFd = ev3_serial_open_file( EV3_SERIAL_BT );
	assert( m_serialFd != NULL );
}

/**
 * @brief デストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
Bluetooth::~Bluetooth()
{
	fclose( m_serialFd );
}

/**
 * @brief シングルトンなインスタンスを取得
 * @author 平井 景
 * @date 2019/06/26
 * @return Bluetoothクラスインスタンス
 */
Bluetooth& Bluetooth::getInstance()
{
	static Bluetooth m_instance;
	return m_instance;
}

/**
 * @brief Bluetooth受信タスクの開始
 * @author 平井 景
 * @date 2019/06/26
 * @return 処理の成否
 */
bool Bluetooth::start()
{
	ER rtn = act_tsk( BT_TASK );
	return ( E_OK == rtn );
}

/**
 * @brief Bluetooth受信タスクの停止
 * @author 平井 景
 * @date 2019/06/26
 * @return 処理の成否
 */
bool Bluetooth::stop()
{
	ER rtn = ter_tsk( BT_TASK );
	return ( E_OK == rtn );
}

/**
 * @brief Bluetooth受信処理(0msec周期)
 * @author 平井 景
 * @date 2019/06/26
 */
void Bluetooth::receiveLoop()
{
	while(1) {
		char buf[SETTING_STR_LEN] = {};
		// 受信
		fgets( buf, (SETTING_STR_LEN - 1), m_serialFd );	// 末尾（終端文字手前）に改行文字が入ることに注意
		Config::getInstance().set( buf );
		// エコーバック
		fputs( buf, m_serialFd );
	}
}

/**
 * @brief Bluetooth送信
 * @author 平井 景
 * @date 2019/06/26
 * @param [in] data 送信文字列
 */
void Bluetooth::send( const char* data )
{
	fputs( data, m_serialFd );
}



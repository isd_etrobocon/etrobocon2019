/**
 * @file Config.cpp
 * @brief 設定を管理
 * @author 平井 景
 * @date 2019/06/26
 */

#include "Config.h"
#include "Log.h"
#include <string.h>

static const char CMD_START[] = "Start\n";
static const char CMD_GET_ALL_SETTINGS[] = "AllSettings\n";
static const char CMD_L_COURSE[] = "Course : L\n";
static const char CMD_R_COURSE[] = "Course : R\n";
static const char CMD_STORE_LOG[] = "StoredLogs\n";
static const char CMD_BALANCERUN_TARGET_REFLECT[] = "BalanceRunTargetReflect : ";	// 前方一致比較なので改行文字は不要
static const char CMD_TAILRUN_TARGET_REFLECT[] = "TailRunTargetReflect : ";	// 前方一致比較なので改行文字は不要
static const char CMD_BACKBENDRUN_TARGET_REFLECT[] = "BackbendRunTargetReflect : ";	// 前方一致比較なので改行文字は不要

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
Config::Config() :
	m_mtxId(0),
	m_isStarted(false),
	m_course(Course_L),
	m_balanceRunTargetReflect(0),
	m_tailRunTargetReflect(0),
	m_backbendRunTargetReflect(0)
{
}

/**
 * @brief デストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
Config::~Config()
{
}

/**
 * @brief シングルトンなインスタンスを取得
 * @author 平井 景
 * @date 2019/06/26
 * @return Configクラスインスタンス
 */
Config& Config::getInstance()
{
	static Config m_instance;
	return m_instance;
}

/**
 * @brief 設定値更新
 * @author 平井 景
 * @date 2019/06/26
 * @param [in] data 設定文字列
 */
void Config::set( const char* data )
{
	lock();
	// スタート指示
	if( 0 == strncmp( data, CMD_START, SETTING_STR_LEN ) ) {
		DEBUG_LOG
		m_isStarted = true;
	}
	// 全設定値表示指示
	if( 0 == strncmp( data, CMD_GET_ALL_SETTINGS, SETTING_STR_LEN ) ) {
		DEBUG_LOG
		printAllSettings();
	}
	// Lコース指定
	if( 0 == strncmp( data, CMD_L_COURSE, SETTING_STR_LEN ) ) {
		DEBUG_LOG
		m_course = Course_L;
	}
	// Rコース指定
	if( 0 == strncmp( data, CMD_R_COURSE, SETTING_STR_LEN ) ) {
		DEBUG_LOG
		m_course = Course_R;
	}
	// 倒立走行目標輝度
	if( 0 == strncmp( data, CMD_BALANCERUN_TARGET_REFLECT, (sizeof(CMD_BALANCERUN_TARGET_REFLECT) - 1) ) ) {
		DEBUG_LOG
		unsigned int reflect = 0;
		int scanNum = sscanf( data, "BalanceRunTargetReflect : %u", &reflect );
		if( 1 != scanNum ) {
			DEBUG_LOG
		}
		m_balanceRunTargetReflect = static_cast<uint8_t>(reflect);
	}
	// 尻尾走行目標輝度
	if( 0 == strncmp( data, CMD_TAILRUN_TARGET_REFLECT, (sizeof(CMD_TAILRUN_TARGET_REFLECT) - 1) ) ) {
		DEBUG_LOG
		unsigned int reflect = 0;
		int scanNum = sscanf( data, "TailRunTargetReflect : %u", &reflect );
		if( 1 != scanNum ) {
			DEBUG_LOG
		}
		m_tailRunTargetReflect = static_cast<uint8_t>(reflect);
	}
	// 後傾走行目標輝度
	if( 0 == strncmp( data, CMD_BACKBENDRUN_TARGET_REFLECT, (sizeof(CMD_BACKBENDRUN_TARGET_REFLECT) - 1) ) ) {
		DEBUG_LOG
		unsigned int reflect = 0;
		int scanNum = sscanf( data, "BackbendRunTargetReflect : %u", &reflect );
		if( 1 != scanNum ) {
			DEBUG_LOG
		}
		m_backbendRunTargetReflect = static_cast<uint8_t>(reflect);
	}
	// 蓄積ログ出力
	if( 0 == strncmp( data, CMD_STORE_LOG, SETTING_STR_LEN ) ) {
		DEBUG_LOG
		Log::getInstance().printStoredLogs();
	}
	unlock();
}

/**
 * @brief スタート指令確認
 * @author 平井 景
 * @date 2019/06/26
 * @retval true スタートが指令された
 * @retval false スタート指令はされていない
 */
bool Config::isStarted()
{
	bool rtn;
	lock();
	rtn = m_isStarted;
	unlock();
	return rtn;
}

/**
 * @brief コース設定取得
 * @author 平井 景
 * @date 2019/06/26
 * @retval Course_L Lコース設定
 * @retval Course_R Rコース設定
 */
Course Config::getCourse()
{
	lock();
	Course rtn = m_course;
	unlock();
	return rtn;
}

/**
 * @brief 倒立走行輝度目標値取得
 * @author 平井 景
 * @date 2019/06/26
 * @return 倒立走行輝度目標値
 */
uint8_t Config::getBalanceRunTargetReflect()
{
	lock();
	uint8_t rtn = m_balanceRunTargetReflect;
	unlock();
	return rtn;
}
/**
 * @brief 尻尾走行輝度目標値取得
 * @author 平井 景
 * @date 2019/08/02
 * @return 尻尾走行輝度目標値
 */
uint8_t Config::getTailRunTargetReflect()
{
	lock();
	uint8_t rtn = m_tailRunTargetReflect;
	unlock();
	return rtn;
}

/**
 * @brief 後傾走行輝度目標値取得
 * @author 平井 景
 * @date 2019/08/02
 * @return 後傾走行輝度目標値
 */
uint8_t Config::getBackbendRunTargetReflect()
{
	lock();
	uint8_t rtn = m_backbendRunTargetReflect;
	unlock();
	return rtn;
}

/**
 * @brief ミューテックスロック
 * @author 平井 景
 * @date 2019/06/26
 */
void Config::lock()
{
	if ( E_OK != loc_mtx( CONFIG_MTX ) ) {
		DEBUG_LOG
	}
}

/**
 * @brief ミューテックス解放
 * @author 平井 景
 * @date 2019/06/26
 */
void Config::unlock()
{
	if ( E_OK != unl_mtx( CONFIG_MTX ) ) {
		DEBUG_LOG
	}
}

/**
 * @brief 全設定値ログ送信
 * @author 平井 景
 * @date 2019/08/10
 */
void Config::printAllSettings()
{
	char sendMsg[32] = {};

	// L/Rコース設定
	if( Course_L == m_course ) {
		snprintf( sendMsg, sizeof(sendMsg), "Course : L" );
	}
	else {
		snprintf( sendMsg, sizeof(sendMsg), "Course : R" );
	}
	DEBUG_LOGF( "%s", sendMsg );

	// 倒立走行目標輝度
	memset( sendMsg, 0, sizeof(sendMsg) );
	snprintf( sendMsg, sizeof(sendMsg), "BalanceRunTargetReflect : %u", m_balanceRunTargetReflect );
	DEBUG_LOGF( "%s", sendMsg );

	// 尻尾走行目標輝度
	memset( sendMsg, 0, sizeof(sendMsg) );
	snprintf( sendMsg, sizeof(sendMsg), "TailRunTargetReflect : %u", m_tailRunTargetReflect );
	DEBUG_LOGF( "%s", sendMsg );

	// 後傾走行目標輝度
	memset( sendMsg, 0, sizeof(sendMsg) );
	snprintf( sendMsg, sizeof(sendMsg), "BackbendRunTargetReflect : %u", m_backbendRunTargetReflect );
	DEBUG_LOGF( "%s", sendMsg );
}

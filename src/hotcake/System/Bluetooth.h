/**
 * @file Bluetooth.h
 * @brief Bluetooth通信
 * @author 平井 景
 * @date 2019/06/26
 */

#ifndef __BLUETOOTH_H__
#define __BLUETOOTH_H__

#include <ev3api.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Bluetooth受信タスク
 * @author 平井 景
 * @date 2019/06/26
 * @param [in] exinf 未使用
 */
extern void btTask(intptr_t exinf);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
#include <ev3api.h>

/**
 * @brief Bluetooth送受信クラス
 * @author 平井 景
 * @date 2019/06/26
 */
class Bluetooth
{
public:
	/**
	 * @brief シングルトンなインスタンスを取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return Bluetoothクラスインスタンス
	 */
	static Bluetooth& getInstance();

	/**
	 * @brief Bluetooth受信タスクの開始
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return 処理の成否
	 */
	bool start();

	/**
	 * @brief Bluetooth受信タスクの停止
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return 処理の成否
	 */
	bool stop();

	/**
	 * @brief Bluetooth受信処理(0msec周期)
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	void receiveLoop();

	/**
	 * @brief Bluetooth送信
	 * @author 平井 景
	 * @date 2019/06/26
	 * @param [in] data 送信文字列
	 */
	void send( const char* data );

private:
	/**
	 * @brief コンストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	Bluetooth();

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	~Bluetooth();

	/**
	 * @brief コピーコンストラクタ
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Bluetooth( const Bluetooth& );

	/**
	 * @brief コピー代入演算子
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Bluetooth& operator=( const Bluetooth& );

	FILE* m_serialFd;	//! シリアル通信ファイルディスクリプタ
};
#endif

#endif // __BLUETOOTH_H__

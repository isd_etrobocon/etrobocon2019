/**
 * @file Config.h
 * @brief 設定を管理
 * @author 平井 景
 * @date 2019/06/26
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <ev3api.h>

//! L/Rコース設定
enum Course
{
	Course_L = 0,	//! Lコース
	Course_R		//! Rコース
};

static const int SETTING_STR_LEN = 256;

/**
 * @brief 設定管理クラス
 * @author 平井 景
 * @date 2019/06/26
 */
class Config
{
public:
	/**
	 * @brief シングルトンなインスタンスを取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return Configクラスインスタンス
	 */
	static Config& getInstance();

	/**
	 * @brief 設定値更新
	 * @author 平井 景
	 * @date 2019/06/26
	 * @param [in] data 設定文字列
	 */
	void set( const char* data );

	/**
	 * @brief スタート指令確認
	 * @author 平井 景
	 * @date 2019/06/26
	 * @retval true スタートが指令された
	 * @retval false スタート指令はされていない
	 */
	bool isStarted();

	/**
	 * @brief コース設定取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @retval Course_L Lコース設定
	 * @retval Course_R Rコース設定
	 */
	Course getCourse();

	/**
	 * @brief 倒立走行輝度目標値取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return 倒立走行輝度目標値
	 */
	uint8_t getBalanceRunTargetReflect();

	/**
	 * @brief 尻尾走行輝度目標値取得
	 * @author 平井 景
	 * @date 2019/08/02
	 * @return 尻尾走行輝度目標値
	 */
	uint8_t getTailRunTargetReflect();

	/**
	 * @brief 後傾走行輝度目標値取得
	 * @author 平井 景
	 * @date 2019/08/02
	 * @return 後傾走行輝度目標値
	 */
	uint8_t getBackbendRunTargetReflect();

private:
	/**
	 * @brief コンストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	Config();

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	~Config();

	/**
	 * @brief コピーコンストラクタ
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Config( const Config& );

	/**
	 * @brief コピー代入演算子
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Config& operator=( const Config& );

	/**
	 * @brief ミューテックスロック
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	void lock();

	/**
	 * @brief ミューテックス解放
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	void unlock();

	/**
	 * @brief 全設定値ログ送信
	 * @author 平井 景
	 * @date 2019/08/10
	 */
	void printAllSettings();

	ER_ID m_mtxId;						//! ミューテックスID

	// 設定項目
	bool m_isStarted;					//! スタート指令状態
	Course m_course;					//! L/Rコース設定
	uint8_t m_balanceRunTargetReflect;	//! 倒立走行輝度目標値
	uint8_t m_tailRunTargetReflect;		//! 尻尾走行輝度目標値
	uint8_t m_backbendRunTargetReflect;	//! 後傾走行輝度目標値
};

#endif // __CONFIG_H__

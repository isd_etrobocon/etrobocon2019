/**
 * @file Log.cpp
 * @brief ログ出力
 * @author 北村 衿子
 * @date 2019/08/14
 */

#include <cstring>
#include <string.h>
#include <stdio.h>
#include "Log.h"

/**
 * @brief コンストラクタ
 * @author 北村 衿子
 * @date 2019/08/14
 */

Log::Log() :
	m_index(0),
	m_isMax(true)
{
	for( int index = 0; index < LOG_ARRAY_MAX_NUM; index++ ) {
		memset( m_storedLogs[index], 0, sizeof(m_storedLogs[index]) );
	}
}

/**
 * @brief デストラクタ
 * @author 北村 衿子
 * @date 2019/08/14
 */
Log::~Log()
{
}

/**
 * @brief シングルトンなインスタンスを取得
 * @author 北村 衿子
 * @date 2019/08/14
 * @return Logクラスインスタンス
 */
Log& Log::getInstance()
{
	static Log m_instance;
	return m_instance;
}

/**
 * @brief ログデータを蓄積
 * @author 北村 衿子
 * @date 2019/08/14
 * @param [in] data 蓄積用ログデータ
 */
void Log::storedLog( char* data )
{
	std::memset( m_storedLogs[ m_index ], 0, sizeof( m_storedLogs[ m_index ] ) );
	std::strncpy( m_storedLogs[ m_index ] , data, ( LOG_ARRAY_MAX_LEN - 1 ) );
	m_index++;

	// 10000ログを超えた場合、先頭メモリに戻って格納
	if( LOG_ARRAY_MAX_NUM == m_index ) {
		m_index = 0;
		m_isMax = true;
	}
}

/**
 * @brief 蓄積ログデータを出力
 * @author 北村 衿子
 * @date 2019/08/15
 */
void Log::printStoredLogs()
{
	int	sendIndex = 0;

	//10000ログ以上の場合
	if( true == m_isMax ) {
		sendIndex = m_index + 1;

		// 改行コードが末尾に来た場合、先頭インデックスを0にする
		if( LOG_ARRAY_MAX_NUM == sendIndex ) {
			sendIndex = 0;
		}

		//先頭インデックスからログ配列末尾までを送信
		for( ; LOG_ARRAY_MAX_NUM > sendIndex; sendIndex++ ) {
			Bluetooth::getInstance().send( m_storedLogs[ sendIndex ] );
		}

		//ログ配列先頭から末尾インデックスまでを送信
		for( sendIndex = 0; m_index > sendIndex; sendIndex++ ) {
			Bluetooth::getInstance().send( m_storedLogs[ sendIndex ] );
		}
	}
	else {
		//10000ログ未満の場合
		for( ; m_index > sendIndex; sendIndex++ ) {
			Bluetooth::getInstance().send( m_storedLogs[ sendIndex ] );
		}
	}
}

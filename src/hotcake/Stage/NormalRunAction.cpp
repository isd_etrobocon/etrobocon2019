/**
* @file NormalRunAction.cpp
* @brief 状態に応じた動作実行
* @author 濱津
* @date 2019/06/30
*/

#include "NormalRunAction.h"
#include "Motion/Odometer.h"
#include "Motion/EmergencyStop.h"
#include "System/Log.h"

static const int TAIL_ANGLE_START = 90;		//! スタート時の尻尾モータ角度[度]
static const int FORWARD_COMMAND_MIN = 50;  //! 前進指令値最小
static const int FORWARD_COMMAND_MAX = 130;	//! 前進指令値最大
static const int SPEED_UP_COUNT = 5;		//! 加速周期カウント

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
NormalRunAction::NormalRunAction() :
	m_forward(FORWARD_COMMAND_MIN),
	m_speedUpCount(0)
{
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
NormalRunAction::~NormalRunAction()
{
}

/**
 * @brief 処理(実体)
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult NormalRunAction::executeImpl()
{
	StageResult stageResult = StageResult_Continue;
	MotionResult tailResult = MotionResult_Error;
	AngleCommand angleCommand;
	SpeedCommand speedCommand;

	if( checkLineColor( COLOR_BLUE ) ) {
		DEBUG_LOG
		stageResult = StageResult_Completed;
		goto LABEL_END;
	}

	angleCommand.angle = TAIL_ANGLE_BALANCE_RUN;

	tailResult = m_tailMove->execute( angleCommand );
	if( MotionResult_Done == tailResult ) {
		m_tailMove->lock();
	}
	else if( MotionResult_Error == tailResult ) {
		stageResult = StageResult_Error;
		goto LABEL_END;
	}
	else {
		// do nothing.
	}

	// 倒立走行安定のため、徐々に速度を上げる
	if( m_forward < FORWARD_COMMAND_MAX ) {
		if( SPEED_UP_COUNT == m_speedUpCount ) {
			m_forward++;
			m_speedUpCount = 0;
		}
		else {
			m_speedUpCount++;
		}
	}
	speedCommand.forward = m_forward;
	speedCommand.reflect = m_reflect;
	speedCommand.angerVelocity = m_angerVelocity;
	speedCommand.mV = m_mV;

	if( MotionResult_Error == m_balanceRun->execute( speedCommand ) ) {
		stageResult = StageResult_Error;
	}

LABEL_END:
	return stageResult;
}

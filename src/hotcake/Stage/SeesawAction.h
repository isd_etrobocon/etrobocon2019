/**
* @file SeesawAction.h
* @brief SeesawAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef SEESAWACTION_H_
#define SEESAWACTION_H_

#include "StageAction.h"

/**
 * @brief SeesawActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class SeesawAction : public StageAction
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	SeesawAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~SeesawAction();

private:

	/**
	 * @brief 処理(実体)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult executeImpl() override;
};

#endif // ! SEESAWACTION_H_

/**
* @file IdleAction.h
* @brief IdleAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef IDLEACTION_H_
#define IDLEACTION_H_

#include "StageAction.h"

/**
 * @brief IdleActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class IdleAction : public StageAction
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	IdleAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~IdleAction();

private:

	/**
	 * @brief 処理(実体)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult executeImpl() override;

	/**
	 * @brief モード・RGB・輝度表示処理
	 * @author 多岐
	 * @date 2019/8/24
	 * @return 状態結果
	 */
	void displayLCD();

};

#endif // ! IDLEACTION_H_

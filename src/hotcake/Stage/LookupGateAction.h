/**
* @file LookupGateAction.h
* @brief LookupGateAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef LOOKUPGATEACTION_H_
#define LOOKUPGATEACTION_H_

#include "StageAction.h"

static const int TAIL_ANGLE_BACKBEND_RUN = 65;		//!後傾走行角度

//! ルックアップゲート状態種別
enum LookupGateActionState {
	LookupGateActionState_DistanceMeasureRunPre = 0,	//!距離計測走行
	LookupGateActionState_DistanceMeasureRun,			//!距離計測走行
	LookupGateActionState_BackbendRunPre,				//!後傾走行準備
	LookupGateActionState_BackbendRun1,					//!後傾走行1
	LookupGateActionState_BackbendRun2,					//!後傾走行2
	LookupGateActionState_Reverse,						//!反転
	LookupGateActionState_TailRunPre,	   				//!尻尾走行準備
	LookupGateActionState_TailRun,		   				//!尻尾走行
};

/**
 * @brief LookupGateActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class LookupGateAction : public StageAction
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	LookupGateAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~LookupGateAction();

private:

	/**
	 * @brief 処理(実体)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult executeImpl() override;

	/**
	 * @brief 距離計測走行準備
	 * @author 濱津
	 * @date 2019/08/18
	 * @return 状態結果
	 */
	StageResult actDistanceMeasureRunPre();

	/**
	 * @brief 距離計測走行
	 * @author 濱津
	 * @date 2019/8/13
	 * @return 状態結果
	 */
	StageResult actDistanceMeasureRun();

	/**
	 * @brief 後傾走行準備
	 * @author 濱津
	 * @date 2019/8/13
	 * @return 状態結果
	 */
	StageResult actBackbendRunPre();

	/**
	 * @brief 後傾走行1
	 * @author 濱津
	 * @date 2019/8/13
	 * @return 状態結果
	 */
	StageResult actBackbendRun1();

	/**
	 * @brief 後傾走行2
	 * @author 平井
	 * @date 2019/9/14
	 * @return 状態結果
	 */
	StageResult actBackbendRun2();

	/**
	 * @brief 反転
	 * @author 濱津
	 * @date 2019/8/13
	 * @return 状態結果
	 */
	StageResult actReverse();

	/**
	 * @brief 尻尾走行準備
	 * @author 濱津
	 * @date 2019/8/13
	 * @return 状態結果
	 */
	StageResult actTailRunPre();

	/**
	 * @brief 尻尾走行
	 * @author 濱津
	 * @date 2019/8/13
	 * @return 状態結果
	 */
	StageResult actTailRun();

	int m_backbendRunCount;  		//! 後傾走行回数
	LookupGateActionState m_state;	//! ルックアップゲート状態管理
	int m_slowDownCount;			//! 減速用周期カウント
};

#endif // ! LOOKUPGATEACTION_H_

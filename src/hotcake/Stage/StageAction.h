/**
* @file StageAction.h
* @brief StageAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef STAGEACTION_H_
#define STAGEACTION_H_

#include <ev3api.h>
#include <TouchSensor.h>
#include <SonarSensor.h>
#include <ColorSensor.h>
#include <GyroSensor.h>
#include "Motion/BalanceRun.h"
#include "Motion/TailRun.h"
#include "Motion/BackBendRun.h"
#include "Motion/Turn.h"
#include "Motion/TailMove.h"
#include "Motion/EmergencyStop.h"

using namespace ev3api;

static const int ERROR_EMERGENCY_COUNTS = 100;  // 異常発生判定周期数
static const int TAIL_ANGLE_TAIL_RUN = 75;		//!尻尾走行角度

//! 状態種別
enum Stage {
	Stage_Initialize = 0,   //!初期化
	Stage_Idle,			 	//!待機中
	Stage_NormalRun,		//!通常走行
	Stage_LookupGate,	   //!ルックアップゲート走行
	Stage_Seasaw,		   //!シーソー走行
	Stage_Parking,		  //!駐車
	Stage_MaxNum,		   //!MAX値
};

//! 状態結果
enum StageResult {
	StageResult_Completed = 0,  //!完了
	StageResult_Continue,	   //!継続
	StageResult_Error,		  //!エラー
};

/**
 * @brief オペレータ ++
 * @author 濱津
 * @date 2019/6/30
 * @return 次のstage状態
 */
Stage& operator++(Stage& stage);

/**
 * @brief StageActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class StageAction
{
public:
	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	StageAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	virtual ~StageAction() = 0;

	/**
	 * @brief 処理
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult execute();

protected:
	/**
	 * @brief 処理(純粋仮想関数)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	virtual StageResult executeImpl() = 0;

	/**
	 * @brief ラインカラー判定
	 * @author 濱津
	 * @date 2019/6/30
	 * @param [in] color 指定カラー
	 * @retval true 指定カラーのラインである
	 * @retval false 指定カラーのラインではない
	 */
	bool checkLineColor( colorid_t color );

	rgb_raw_t m_rgb;						//! 現在RGB値
	uint8_t m_reflect;  					//! 現在輝度
	int16_t m_angerVelocity;				//! 現在角速度
	int m_mV;								//! 現在電圧値
	int16_t m_distance;							//! 前方障害物との距離
	int m_emergencyCounts;					//! 異常発生周期カウント

	static BalanceRun* m_balanceRun;		//! 倒立走行インスタンス
	static TailRun* m_tailRun;				//! 尻尾走行インスタンス
	static BackBendRun* m_backbendRun;		//! 後傾走行インスタンス
	static Turn* m_turn;					//! 旋回インスタンス
	static TailMove* m_tailMove;			//! 尻尾動作インスタンス
	static EmergencyStop* m_emergencyStop;	//! 緊急停止インスタンス

	static GyroSensor* m_gyroSensor;		//! ジャイロセンサ

private:
	/**
	 * @brief ラインカラー判定
	 * @author 平井
	 * @date 2019/8/08
	 * @return 輝度（0〜100）
	 */
	uint8_t convertRgbToReflect();

	/**
	 * @brief 緊急停止判定
	 * @author 濱津
	 * @date 2019/8/17
	 * @return 状態結果
	 */
	bool checkErrorEmergency();

	static SonarSensor* m_sonarSensor;	//! 超音波センサ
	static ColorSensor* m_colorSensor;	//! カラーセンサ
};

#endif // ! STAGEACTION_H_

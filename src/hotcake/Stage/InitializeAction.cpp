/**
* @file InitializeAction.cpp
* @brief 状態に応じた動作実行
* @author 濱津
* @date 2019/06/30
*/

#include "InitializeAction.h"
#include "System/Bluetooth.h"
#include "System/Log.h"
#include "Motion/Odometer.h"

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
InitializeAction::InitializeAction()
{
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
InitializeAction::~InitializeAction()
{

}

/**
 * @brief 処理(実体)
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult InitializeAction::executeImpl()
{
	DEBUG_LOG
	// Bluetooth受信開始
	Bluetooth::getInstance().start();

	return StageResult_Completed;
}

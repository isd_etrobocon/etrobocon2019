/**
* @file stageAction.cpp
* @brief 状態に応じた動作実行
* @author 濱津
* @date 2019/06/30
*/

#include <float.h>
#include "StageAction.h"
#include "ColorSensor.h"
#include "Motion/EmergencyStop.h"
#include "Motion/Odometer.h"
#include "System/Config.h"
#include "System/Log.h"


static const uint8_t REFLECT_MAX = 100;		//! 輝度最大値
#if 0
static const float R_B_RATIO = 2.5f;		//! 赤青比率（京都）
static const float G_B_RATIO = 1.5f;		//! 緑青比率（京都）
#else
static const float R_B_RATIO = 1.5f;		//! 赤青比率（本社）
static const float G_B_RATIO = 1.1f;		//! 緑青比率（本社）
#endif

BalanceRun* StageAction::m_balanceRun = NULL;
TailRun* StageAction::m_tailRun = NULL;
BackBendRun* StageAction::m_backbendRun = NULL;
Turn* StageAction::m_turn = NULL;
TailMove* StageAction::m_tailMove = NULL;
EmergencyStop* StageAction::m_emergencyStop = NULL;
SonarSensor* StageAction::m_sonarSensor = NULL;
ColorSensor* StageAction::m_colorSensor = NULL;
GyroSensor* StageAction::m_gyroSensor = NULL;

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
StageAction::StageAction() :
	m_rgb({}),
	m_reflect(0),
	m_angerVelocity(0),
	m_mV(0),
	m_distance(0),
	m_emergencyCounts(0)
{
	if( NULL == m_balanceRun ) {
		DEBUG_LOG
		m_balanceRun = new BalanceRun();
		if( NULL == m_balanceRun ){
			DEBUG_LOG
		}
	}
	if( NULL == m_tailRun ) {
		DEBUG_LOG
		m_tailRun = new TailRun();
		if( NULL == m_tailRun ){
			DEBUG_LOG
		}
	}
	if( NULL == m_backbendRun ) {
		DEBUG_LOG
		m_backbendRun = new BackBendRun();
		if( NULL == m_backbendRun ){
			DEBUG_LOG
		}
	}
	if( NULL == m_turn ) {
		DEBUG_LOG
		m_turn = new Turn();
		if( NULL == m_turn ){
			DEBUG_LOG
		}
	}
	if( NULL == m_tailMove ) {
		DEBUG_LOG
		m_tailMove = new TailMove();
		if( NULL == m_tailMove ){
			DEBUG_LOG
		}
	}
	if( NULL == m_emergencyStop ) {
		DEBUG_LOG
		m_emergencyStop = new EmergencyStop();
		if( NULL == m_emergencyStop ){
			DEBUG_LOG
		}
	}
	if( NULL == m_sonarSensor ) {
		DEBUG_LOG
		m_sonarSensor = new SonarSensor( PORT_2 );
		if( NULL == m_sonarSensor ){
			DEBUG_LOG
		}
	}
	if( NULL == m_colorSensor ) {
		DEBUG_LOG
		m_colorSensor = new ColorSensor( PORT_3 );
		if( NULL == m_colorSensor ){
			DEBUG_LOG
		}
		// RGB取得モードに設定
		m_colorSensor->getRawColor( m_rgb );
	}
	if( NULL == m_gyroSensor ) {
		DEBUG_LOG
		m_gyroSensor = new GyroSensor( PORT_4 );
		if( NULL == m_gyroSensor ){
			DEBUG_LOG
		}
	}
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
StageAction::~StageAction()
{
}

/**
 * @brief 処理
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult StageAction::execute()
{
	Odometer::getInstance().update();

	m_colorSensor->getRawColor( m_rgb );
	m_reflect = convertRgbToReflect();
	m_angerVelocity = m_gyroSensor->getAnglerVelocity();
	m_mV = ev3_battery_voltage_mV();
	m_distance = m_sonarSensor->getDistance();
	

	// 緊急停止判定
	if( checkErrorEmergency() ){
		DEBUG_LOG
		return StageResult_Error;
	}

	// ステージ固有処理
	StageResult result = executeImpl();

	if( StageResult_Completed == result ) {
		m_tailMove->unlock();
		Odometer::getInstance().reset();
		ev3_speaker_play_tone( NOTE_C5, 500 );
	 }

	return result;
}

/**
 * @brief ラインカラー判定
 * @author 濱津
 * @date 2019/6/30
 * @param [in] color 指定カラー
 * @retval true 指定カラーのラインである
 * @retval false 指定カラーのラインではない
 */
bool StageAction::checkLineColor( colorid_t color )
{
	// ラインカラー判定
	bool result = false;

	uint16_t redRatio = static_cast<uint16_t>(static_cast<float>(m_rgb.r) * R_B_RATIO * 100.0f);
	uint16_t greenRatio = static_cast<uint16_t>(static_cast<float>(m_rgb.g) * G_B_RATIO * 100.0f);
	uint16_t blueRatio = m_rgb.b * 100;

	switch(color) {
		case COLOR_BLUE:
			// 青の場合、RGBのBがRのR_B_RATIO倍以上、かつBがGのG_B_RATIO倍以上になる
			if( ( redRatio < blueRatio ) && ( greenRatio < blueRatio ) /*&& ( m_rgb.b > 50 )*/ ) {
				DEBUG_LOGF("BLUE detected : R=%u, G=%u, B=%u", m_rgb.r, m_rgb.g, m_rgb.b);
				DEBUG_LOGF("red=%u, green=%u, blue=%u", redRatio, greenRatio, blueRatio);
				result = true;
			}
			break;

		default:
			DEBUG_LOG
			break;
	}

	return result;
}

/**
 * @brief ラインカラー判定
 * @author 平井
 * @date 2019/8/08
 * @return 輝度（0〜100）
 */
uint8_t StageAction::convertRgbToReflect()
{
	// グレースケール（≒輝度）へ変換
	// [RGB単純平均]
	// uint8_t reflect = static_cast<uint8_t>((((static_cast<float>(m_rgb.r + m_rgb.g + m_rgb.b) / 3.0f) / 256.0f) * 100.0f) + 0.5f);
	// [ITU-R Rec BT.601 規格]
	// uint8_t reflect = static_cast<uint8_t>(((((0.299f * static_cast<float>(m_rgb.r)) + (0.587f * static_cast<float>(m_rgb.g)) + (0.114f * static_cast<float>(m_rgb.b))) / 256.0f) * 100.0f) + 0.5f);
	// [CIE XYZのYへ変換(輝度保存変換)]
	uint8_t reflect = static_cast<uint8_t>(((((0.2126f * static_cast<float>(m_rgb.r)) + (0.7152f * static_cast<float>(m_rgb.g)) + (0.0722f * static_cast<float>(m_rgb.b))) / 256.0f) * 100.0f) + 0.5f);
	if( REFLECT_MAX < reflect ) {
		reflect = REFLECT_MAX;
	}

	return reflect;
}

/**
 * @brief 緊急停止判定
 * @author 濱津
 * @date 2019/8/17
 * @return 状態結果
 */
bool StageAction::checkErrorEmergency()
{
	bool errorFlg = false;
	// 緊急停止判定
	if( 0 == m_reflect ) {
		if( ERROR_EMERGENCY_COUNTS < m_emergencyCounts ) {
			DEBUG_LOGF("Emergency Counts: %d",m_emergencyCounts);
			EmergencyStop emergencyStop;
			emergencyStop.execute();
			errorFlg = true;
		}
		else {
			m_emergencyCounts++;
		}
	}

	return errorFlg;
}

/**
 * @brief オペレータ ++
 * @author 濱津
 * @date 2019/7/9
 * @return 次のstage状態
 */
Stage& operator++(Stage& stage)
{
	switch (stage) {
		case Stage_Initialize:
		case Stage_Idle:
		case Stage_Parking:
			stage = static_cast<Stage>(static_cast<int>(stage) + 1);
			break;

		case Stage_NormalRun:
			if( Course_L == Config::getInstance().getCourse() ) {
				//Lコース設定
				stage = Stage_Seasaw;
			}
			else {
				//Rコース設定
				stage = Stage_LookupGate;
			}
			break;

		case Stage_LookupGate:
		case Stage_Seasaw:
			stage = Stage_Parking;
			break;

		default:
			break;
	}
	return stage;
}

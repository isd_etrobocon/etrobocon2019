/**
* @file ParkingAction.cpp
* @brief 状態に応じた動作実行(駐車クラス)
* @author 濱津
* @date 2019/06/30
*/

#include "ParkingAction.h"
#include "Motion/Odometer.h"
#include "System/Log.h"

static const int32_t PARKING_RUN_TOTAL_DISTANCE = 230 * 1000;	//! 駐車走行が100mm(距離はアーキテクチャ全体で1000倍mmで統一されているため、定数の方で先に1000倍)

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
ParkingAction::ParkingAction()
{
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
ParkingAction::~ParkingAction()
{
	
}

/**
 * @brief 処理(実体)
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult ParkingAction::executeImpl()
{
	StageResult stageResult = StageResult_Continue;
	PositionCommand positionCommand;
	positionCommand.mm1000times = PARKING_RUN_TOTAL_DISTANCE;
	positionCommand.reflect = m_reflect;

	MotionResult motionResult = MotionResult_Continue;
	motionResult = m_tailRun->execute(positionCommand);

	if(MotionResult_Done == motionResult) {
		stageResult = StageResult_Completed;
		DEBUG_LOG
	}
	else if(MotionResult_Error == motionResult) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}
	else {
		// do nothing
	}

	return stageResult;
}

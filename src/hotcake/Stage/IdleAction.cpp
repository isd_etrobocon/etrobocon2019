/**
* @file SeesawAction.cpp
* @brief 状態に応じた動作実行
* @author 濱津
* @date 2019/06/30
*/

#include <string.h>
#include "IdleAction.h"
#include "NormalRunAction.h"
#include "LookupGateAction.h"
#include "System/Bluetooth.h"
#include "System/Config.h"
#include "System/Log.h"
#include "Motion/Odometer.h"
#include "Motion/Encoder.h"

static const int32_t TAIL_ANGLE_STAND_UP = 88;	// 完全停止時の角度[度]

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
IdleAction::IdleAction()
{
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
IdleAction::~IdleAction()
{

}

/**
 * @brief 処理(実体)
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult IdleAction::executeImpl()
{
	StageResult stageResult = StageResult_Continue;
	MotionResult motionResult = MotionResult_Continue;
	AngleCommand angleCommand;

	if( Config::getInstance().isStarted() ) {
		DEBUG_LOG
		Bluetooth::getInstance().stop();
		m_gyroSensor->reset();
		Encoder::getInstance().reset();
		stageResult = StageResult_Completed;
		goto LABEL_END;
	}

	angleCommand.angle = TAIL_ANGLE_STAND_UP;
	motionResult = m_tailMove->execute( angleCommand );
	if( MotionResult_Done == motionResult ) {
		m_tailMove->lock();
	}
	else if( MotionResult_Error == motionResult ) {
		stageResult = StageResult_Error;
	}
	else {
		// do nothing
	}

LABEL_END:
	return stageResult;
}

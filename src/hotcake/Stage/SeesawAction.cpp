/**
* @file SeesawAction.cpp
* @brief 状態に応じた動作実行
* @author 濱津
* @date 2019/06/30
*/

#include "SeesawAction.h"

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
SeesawAction::SeesawAction()
{
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
SeesawAction::~SeesawAction()
{

}

/**
 * @brief 処理(実体)
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult SeesawAction::executeImpl()
{
	//処理開始
	//code
	//処理終了
	return StageResult_Completed;
}

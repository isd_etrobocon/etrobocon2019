/**
* @file InitializeAction.h
* @brief InitializeAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef INITIALIZEACTION_H_
#define INITIALIZEACTION_H_

#include "StageAction.h"

/**
 * @brief InitializeActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class InitializeAction : public StageAction
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	InitializeAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~InitializeAction();

private:

	/**
	 * @brief 処理(実体)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult executeImpl() override;
};

#endif // ! INITIALIZEACTION_H_

/**
* @file ParkingAction.h
* @brief ParkingAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef PARKINGACTION_H_
#define PARKINGACTION_H_

#include "StageAction.h"

/**
 * @brief ParkingActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class ParkingAction : public StageAction
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	ParkingAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~ParkingAction();

private:

	/**
	 * @brief 処理(実体)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult executeImpl() override;
};

#endif // ! PARKINGACTION_H_

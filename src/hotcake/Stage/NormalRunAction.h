/**
* @file NormalRunAction.h
* @brief NormalRunAction
* @author 濱津
* @date 2019/06/30
*/
#ifndef NORMALRUNACTION_H_
#define NORMALRUNACTION_H_

#include "StageAction.h"

static const int TAIL_ANGLE_BALANCE_RUN = 3;  //! バランス走行時の尻尾モータ角度[度]

/**
 * @brief NormalRunActionクラス
 * @author 濱津信康
 * @date 2019/06/30
 */
class NormalRunAction : public StageAction
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	NormalRunAction();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~NormalRunAction();

private:

	/**
	 * @brief 処理(実体)
	 * @author 濱津
	 * @date 2019/6/30
	 * @return 状態結果
	 */
	StageResult executeImpl() override;

	int m_forward;  	//! 前進指令値
	int m_speedUpCount;	//! 加速用周期カウント
};

 #endif // ! NORMALRUNACTION_H_
 
/**
* @file LookupGateAction.cpp
* @brief 状態に応じた動作実行
* @author 濱津
* @date 2019/06/30
*/

#include "LookupGateAction.h"
#include "Motion/Motion.h"
#include "Motion/Odometer.h"
#include "Motion/Encoder.h"
#include "System/Log.h"

static const int16_t FORWARD_OBSTACLE_DISTANCE_VALUE = 20;		//! 前方障害物までの距離値
static const int16_t NOTHING_OBSTACLE_VALUE = 255;				//! 前方障害物までの距離値
static const int BACK_BEND_RUN_MAX_COUNT = 5;					//! 後傾走行MAX回数
static const int ANGLE_REVERSE = 180;							//! 反転角度
static const int MOST_SLOW_DOWN_FORWARD = 10;					//! 最徐行
static const int SLOW_DOWN_FORWARD = 20;						//! 徐行
static const int BRAKE_FORWARD = 10;							//! 減速
static const int ACCELL_FORWARD = 100;							//! 加速
static const int SLOW_DOWN_COUNT = 250;							//! 徐行周期数
static const int32_t BACK_BEND_RUN_DISTANCE = 350 * 1000;		//! 後傾走行位置指令値(単位はmm*1000)

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
LookupGateAction::LookupGateAction():
	m_backbendRunCount(0),
	m_state(LookupGateActionState_DistanceMeasureRunPre),
	m_slowDownCount(0)
{
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
LookupGateAction::~LookupGateAction()
{
}

/**
 * @brief 処理(実体)
 * @author 濱津
 * @date 2019/6/30
 * @return 状態結果
 */
StageResult LookupGateAction::executeImpl()
{
	StageResult allCompletedStageResult = StageResult_Continue;
	StageResult oneStageResult = StageResult_Continue;

	switch (m_state) {
		case LookupGateActionState_DistanceMeasureRunPre:
			oneStageResult = actDistanceMeasureRunPre();

			if(StageResult_Completed == oneStageResult) {
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				m_state = LookupGateActionState_DistanceMeasureRun;
				ev3_speaker_play_tone( NOTE_C5, 100 );
				DEBUG_LOG
			}
			break;

		case LookupGateActionState_DistanceMeasureRun:
			oneStageResult = actDistanceMeasureRun();

			if(StageResult_Completed == oneStageResult) {
				m_tailMove->unlock();
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				m_state = LookupGateActionState_BackbendRunPre;
				ev3_speaker_play_tone( NOTE_C5, 100 );
				DEBUG_LOG
			}
			break;

		case LookupGateActionState_BackbendRunPre:
			oneStageResult = actBackbendRunPre();

			if(StageResult_Completed == oneStageResult) {
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				m_state = LookupGateActionState_BackbendRun1;
				ev3_speaker_play_tone( NOTE_C5, 100 );
				DEBUG_LOG
			}
			break;

		case LookupGateActionState_BackbendRun1:
			oneStageResult = actBackbendRun1();

			if(StageResult_Completed == oneStageResult) {
				DEBUG_LOG
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				m_state = LookupGateActionState_BackbendRun2;
				ev3_speaker_play_tone( NOTE_C5, 100 );
			}
			break;

		case LookupGateActionState_BackbendRun2:
			oneStageResult = actBackbendRun2();

			if(StageResult_Completed == oneStageResult) {
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				if(BACK_BEND_RUN_MAX_COUNT > m_backbendRunCount) {
					DEBUG_LOG
					m_state = LookupGateActionState_Reverse;
					ev3_speaker_play_tone( NOTE_C5, 100 );
				}
				else {
					DEBUG_LOG
					m_tailMove->unlock();
					m_state = LookupGateActionState_TailRunPre;
					ev3_speaker_play_tone( NOTE_C5, 100 );
				}
			}
			break;

		case LookupGateActionState_Reverse:
			oneStageResult = actReverse();

			if(StageResult_Completed == oneStageResult) {
				m_backbendRun->resetSteeringPIDParam();
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				m_state = LookupGateActionState_BackbendRun1;
				ev3_speaker_play_tone( NOTE_C5, 100 );
				DEBUG_LOG
			}
			break;

		case LookupGateActionState_TailRunPre:
			oneStageResult = actTailRunPre();

			if(StageResult_Completed == oneStageResult) {
				m_tailRun->resetSteeringPIDParam();
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				m_state = LookupGateActionState_TailRun;
				ev3_speaker_play_tone( NOTE_C5, 100 );
				DEBUG_LOG
			}
			break;

		case LookupGateActionState_TailRun:
			oneStageResult = actTailRun();

			if(StageResult_Completed == oneStageResult) {
				DEBUG_LOG
				Odometer::getInstance().reset();
				Encoder::getInstance().reset();
				allCompletedStageResult = StageResult_Completed;
			}
			break;
		
		default:
			DEBUG_LOG
			break;
	}

	if(StageResult_Error == oneStageResult) {
		allCompletedStageResult = StageResult_Error;
	}

	return allCompletedStageResult;
}

/**
 * @brief 距離計測走行準備
 * @author 濱津
 * @date 2019/08/18
 * @return 状態結果
 */
StageResult LookupGateAction::actDistanceMeasureRunPre()
{
	StageResult stageResult = StageResult_Continue;

	SpeedCommand speedCommand;
	speedCommand.forward = BRAKE_FORWARD;
	speedCommand.reflect = m_reflect;
	speedCommand.angerVelocity = m_angerVelocity;
	speedCommand.mV = m_mV;
	static bool isOutward = false;	// 外向きフラグ

	//尻尾角度を指示
	AngleCommand angleCommand;
	angleCommand.angle = TAIL_ANGLE_TAIL_RUN;
	MotionResult motionResult = MotionResult_Error;
	motionResult = m_tailMove->execute(angleCommand);

	if(MotionResult_Error == motionResult){
		stageResult = StageResult_Error;
		DEBUG_LOG
	}
	else if( MotionResult_Done == motionResult ){
		m_tailMove->lock();
		m_slowDownCount++;
		// エッジが変わらぬよう、右向き（ラインの外側向き）の時に尻尾倒立姿勢へ
		if( 0 <= m_balanceRun->getTurn() ) {
			isOutward = true;
		}
		// 走行体がエッジの外側を向いている、かつ
		// 減速用時間が経過
		if( isOutward && ( SLOW_DOWN_COUNT <= m_slowDownCount ) ) {
			DEBUG_LOGF("m_slowDownCount=%d",m_slowDownCount);
			DEBUG_LOGF("m_angerVelocity=%d",m_angerVelocity);
			m_slowDownCount = 0;
			speedCommand.forward = ACCELL_FORWARD;
			stageResult = StageResult_Completed;
		}
	}
	else {
	}

	if(MotionResult_Error == m_balanceRun->execute(speedCommand)) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}

	return stageResult;
}

/**
 * @brief 距離計測走行
 * @author 濱津
 * @date 2019/8/13
 * @return 状態結果
 */
StageResult LookupGateAction::actDistanceMeasureRun()
{
	StageResult stageResult = StageResult_Continue;
	SpeedCommand speedCommand;
	speedCommand.reflect = m_reflect;

	if( ( 0 != m_distance ) && 
		( FORWARD_OBSTACLE_DISTANCE_VALUE >= m_distance ) &&
		( 0 <= m_tailRun->getTurn() ) ) {	// エッジが変わらぬよう、右向き（ラインの外側向き）の時に後傾姿勢へ
		DEBUG_LOGF("m_distance=%d", m_distance);
		speedCommand.forward = 0;
		stageResult = StageResult_Completed;
	}
	else {
		speedCommand.forward = SLOW_DOWN_FORWARD;
	}

	if( MotionResult_Error == m_tailRun->execute(speedCommand)) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}

	return stageResult;
}

/**
 * @brief 後傾走行準備
 * @author 濱津
 * @date 2019/8/13
 * @return 状態結果
 */
StageResult LookupGateAction::actBackbendRunPre()
{
	StageResult stageResult = StageResult_Continue;
	MotionResult motionResult = MotionResult_Continue;

	SpeedCommand speedCommand;
	speedCommand.forward = MOST_SLOW_DOWN_FORWARD;
	speedCommand.reflect = m_reflect;

	motionResult = m_tailRun->execute(speedCommand);
	if( MotionResult_Error == motionResult) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}
	else {
		// 後傾走行用角度を指示
		AngleCommand angleCommand;
		angleCommand.angle = TAIL_ANGLE_BACKBEND_RUN;

		motionResult = m_tailMove->execute(angleCommand);

		if(MotionResult_Done == motionResult) {
			m_tailMove->lock();
			stageResult = StageResult_Completed;
			DEBUG_LOG
		}
		else if(MotionResult_Error == motionResult){
			stageResult = StageResult_Error;
			DEBUG_LOG
		}
	}

	return stageResult;
}

/**
 * @brief 後傾走行1
 * @author 濱津
 * @date 2019/8/13
 * @return 状態結果
 */
StageResult LookupGateAction::actBackbendRun1()
{
	PositionCommand positionCommand;
	positionCommand.mm1000times = BACK_BEND_RUN_DISTANCE;
	positionCommand.reflect = m_reflect;

	MotionResult motionResult = MotionResult_Continue;
	motionResult = m_backbendRun->execute(positionCommand);

	StageResult stageResult = StageResult_Continue;
	if(MotionResult_Done == motionResult) {
		stageResult = StageResult_Completed;
		DEBUG_LOG
	}
	else if(MotionResult_Error == motionResult) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}
	else {
		// do nothing
	}

	return stageResult;
}

/**
 * @brief 後傾走行2
 * @author 平井
 * @date 2019/9/14
 * @return 状態結果
 */
StageResult LookupGateAction::actBackbendRun2()
{
	StageResult stageResult = StageResult_Continue;
	SpeedCommand speedCommand;
	speedCommand.reflect = m_reflect;

	// 後傾姿勢で障害物との距離255(=見失った)ということは、ゲートを潜り切ったと言える
	if( m_distance >= NOTHING_OBSTACLE_VALUE ) {
		DEBUG_LOGF("m_distance=%d", m_distance);
		m_backbendRunCount++;
		speedCommand.forward = 0;
		stageResult = StageResult_Completed;
	}
	else {
		speedCommand.forward = SLOW_DOWN_FORWARD;
	}

	if( MotionResult_Error == m_backbendRun->execute(speedCommand)) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}

	return stageResult;
}

/**
 * @brief 反転
 * @author 濱津
 * @date 2019/8/13
 * @return 状態結果
 */
StageResult LookupGateAction::actReverse()
{
	//180度旋回指示
	AngleCommand angleCommand;
	angleCommand.angle = ANGLE_REVERSE;
	MotionResult motionResult = MotionResult_Continue;
	motionResult = m_turn->execute(angleCommand);

	StageResult stageResult = StageResult_Continue;

	if(MotionResult_Done ==	motionResult) {
		stageResult = StageResult_Completed;
		m_backbendRun->reverseDirection();
		DEBUG_LOG
	}
	else if(MotionResult_Error == motionResult) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}
	else {
		// do nothing
	}

	return stageResult;
}

/**
 * @brief 尻尾走行準備
 * @author 濱津
 * @date 2019/8/13
 * @return 状態結果
 */
StageResult LookupGateAction::actTailRunPre()
{
	MotionResult motionResult = MotionResult_Continue;
	StageResult stageResult = StageResult_Continue;
	SpeedCommand speedCommand;
	AngleCommand angleCommand;

	speedCommand.forward = -5;
	speedCommand.reflect = m_reflect;

	motionResult = m_backbendRun->execute(speedCommand);
	if(MotionResult_Error == motionResult) {
		stageResult = StageResult_Error;
		DEBUG_LOG
		goto LABEL_END;
	}

	//尻尾走行用角度を指示
	angleCommand.angle = TAIL_ANGLE_TAIL_RUN;
	motionResult = m_tailMove->execute(angleCommand);

	if(MotionResult_Done == motionResult) {
		DEBUG_LOG
		m_tailMove->lock();
		stageResult = StageResult_Completed;

		speedCommand.forward = 0;
		motionResult = m_backbendRun->execute(speedCommand);

		if(MotionResult_Error == motionResult) {
			stageResult = StageResult_Error;
			DEBUG_LOG
		}
	}
	else if(MotionResult_Error == motionResult) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}
	else {
		// do nothing
	}

LABEL_END:
	return stageResult;
}

/**
 * @brief 尻尾走行
 * @author 濱津
 * @date 2019/8/13
 * @return 状態結果
 */
StageResult LookupGateAction::actTailRun()
{
	StageResult stageResult = StageResult_Continue;
	SpeedCommand speedCommand;
	speedCommand.reflect = m_reflect;

	if( checkLineColor( COLOR_BLUE ) ) {
		speedCommand.forward = 0;
		stageResult = StageResult_Completed;
		DEBUG_LOG
	}
	else {
		speedCommand.forward = MOST_SLOW_DOWN_FORWARD;
	}

	if(MotionResult_Error == m_tailRun->execute(speedCommand)) {
		stageResult = StageResult_Error;
		DEBUG_LOG
	}

	return stageResult;
}

/**
* @file app.h
* @brief メインタスク
* @author 濱津
* @date 2019/06/30
*/
#ifndef APP_H_
#define APP_H_


#ifdef __cplusplus
extern "C" {
#endif

#ifndef STACK_SIZE
#define STACK_SIZE	  4096		/* タスクのスタックサイズ */
#endif /* STACK_SIZE */


/**
 * @brief メインタスク
 * @author 濱津
 * @date 2019/6/30
 * @param [in] exinf 未使用
 */
extern void mainTask(intptr_t exinf);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
#include "Stage/StageAction.h"

/**
 * @brief Scenarioクラス
 * @author 濱津
 * @date 2019/6/30
 */
class Scenario 
{

public:
	/**
	 * @brief シングルトンなインスタンスを取得
	 * @author 濱津
	 * @date 2019/6/30
	 * @return Scenarioクラスインスタンス
	 */
	static Scenario& getInstance();

	/**
	 * @brief シナリオループ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	void scenarioLoop();

private:
	/**
	 * @brief コンストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	Scenario();

	/**
	 * @brief デストラクタ
	 * @author 濱津
	 * @date 2019/6/30
	 */
	~Scenario();

	/**
	 * @brief コピーコンストラクタ
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Scenario(const Scenario&);

	/**
	 * @brief コピー代入演算子
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Scenario& operator=(const Scenario&);

	/**
	 * @brief stage状態遷移
	 * @author 濱津
	 * @date 2019/6/30
	 */
	void transitStage();

	StageAction* m_stageActions[Stage_MaxNum]; //! stageActionリスト管理
	Stage m_stage;		//! 状態管理
};
#endif

#endif // ! APP_H_
/**
 * @file TailMove.cpp
 * @brief 尻尾動作
 * @author 平井 景
 * @date 2019/06/26
 */

#include <ev3api.h>
#include "System/Log.h"
#include "TailMove.h"
#include "Encoder.h"

static const int PWM_ABS_MAX = 60;	//! モーター制御PWM絶対最大値

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
TailMove::TailMove() :
	m_isLock(false)
{
}

/**
 * @brief デストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
TailMove::~TailMove()
{
}

/**
 * @brief 動作実行
 * @author 平井 景
 * @date 2019/06/26
 * @param [in] param 角度指令
 * @return Motion実行結果
 */
MotionResult TailMove::execute( AngleCommand& param )
{
	MotionResult result = MotionResult_Continue;

	if( m_isLock ){
		STORED_LOG
		result = MotionResult_Done;
	}
	else {
		Encoder& encoder = Encoder::getInstance();

		int pwm = param.angle - static_cast<int>(encoder.getTailCounts());
		if( pwm > PWM_ABS_MAX ) {
			pwm = PWM_ABS_MAX;
		}
		else if( pwm < (PWM_ABS_MAX * (-1))) {
			pwm = (PWM_ABS_MAX * (-1));
		}
		else {
			// do nothing
		}

		if( pwm == 0 ) {
			m_tailMotor->stop();
			result = MotionResult_Done;
		}
		else {
			m_tailMotor->setPWM( pwm ) ;
			result = MotionResult_Continue;
		}
	}

	return result;
}

/**
 * @brief ブレーキロック
 * @author 平井 景
 * @date 2019/08/25
 */
void TailMove::lock()
{
	m_isLock = true;
}

/**
 * @brief ブレーキロック解除
 * @author 平井 景
 * @date 2019/08/25
 */
void TailMove::unlock()
{
	m_isLock= false;
}

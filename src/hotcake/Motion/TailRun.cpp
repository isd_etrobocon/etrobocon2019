/**
 * @file TailRun.cpp
 * @brief 尻尾走行
 * @author 濱津 信康
 * @date 2019/08/18
 */

#include "TailRun.h"
#include "Motion/Odometer.h"
#include "System/Log.h"

static const float GAIN_STEERING_KP = 10.0f;	//! 操舵角Pゲイン値
static const float GAIN_STEERING_KI = 0.0f;		//! 操舵角Iゲイン値
static const float GAIN_STEERING_KD = 0.0f;		//! 操舵角Dゲイン値
static const int PARKING_FORWARD = 10;			//! 駐車速度	

static const PIDGain PID_GAIN(GAIN_STEERING_KP, GAIN_STEERING_KI, GAIN_STEERING_KD);	// 操舵角PIDゲイン

/**
 * @brief コンストラクタ
 * @author 濱津 信康
 * @date 2019/08/18
 */
TailRun::TailRun() : 
	ThreePointRun(PID_GAIN)
{
}

/**
 * @brief デストラクタ
 * @author 濱津 信康
 * @date 2019/08/18
 */
TailRun::~TailRun()
{
}

/**
 * @brief 動作実行（速度指令）
 * @author 濱津 信康
 * @date 2019/08/18
 * @param [in] param 速度指令
 * @return Motion実行結果
 */
MotionResult TailRun::execute(SpeedCommand& param)
{
	uint8_t targetReflect = Config::getInstance().getTailRunTargetReflect();
	float steeringAngle = calculateSteeringPID(targetReflect, param.reflect);
	m_turnRatio = static_cast<int>(steeringAngle);

	if(0 == param.forward) {
		m_leftMotor->stop();
		m_rightMotor->stop();
	}
	else {
		m_steering->setPower(param.forward, static_cast<int>(steeringAngle));
	}

	return MotionResult_Continue;
}

/**
 * @brief 動作実行（位置指令）
 * @author 濱津 信康
 * @date 2019/08/31
 * @param [in] param 位置指令
 * @return Motion実行結果
 */
MotionResult TailRun::execute(PositionCommand& param)
{
	MotionResult motionResult = MotionResult_Continue;
	int32_t totalDistance = Odometer::getInstance().getTotalDistance();
	uint8_t targetReflect = Config::getInstance().getTailRunTargetReflect();
	float steeringAngle = calculateSteeringPID(targetReflect, param.reflect);
	m_turnRatio = static_cast<int>(steeringAngle);

	//走行距離 >= param.mm1000times
	if( totalDistance >= param.mm1000times) {
		m_leftMotor->stop();
		m_rightMotor->stop();
		motionResult = MotionResult_Done;
	}
	else {
		m_steering->setPower(PARKING_FORWARD, static_cast<int>(steeringAngle));
	}
 
	return motionResult;
}

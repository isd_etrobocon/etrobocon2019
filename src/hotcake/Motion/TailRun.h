/**
 * @file TailRun.h
 * @brief 尻尾走行
 * @author 濱津 信康
 * @date 2019/08/18
 */

#ifndef __TAIL_RUN_H__
#define __TAIL_RUN_H__

#include "ThreePointRun.h"

/**
 * @file TailRun.h
 * @brief 尻尾走行クラス
 * @author 濱津 信康
 * @date 2019/08/18
 */
class TailRun : public ThreePointRun
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津 信康
	 * @date 2019/08/18
	 */
	TailRun();

	/**
	 * @brief デストラクタ
	 * @author 濱津 信康
	 * @date 2019/08/18
	 */
	~TailRun();

	/**
	 * @brief 動作実行（速度指令）
	 * @author 濱津 信康
	 * @date 2019/08/18
	 * @param [in] param 速度指令
	 * @return Motion実行結果
	 */
	MotionResult execute(SpeedCommand& param) override;

	/**
	 * @brief 動作実行（速度指令）
	 * @author 濱津 信康
	 * @date 2019/08/31
	 * @param [in] param 位置指令
	 * @return Motion実行結果
	 */
	MotionResult execute(PositionCommand& param) override;

};
#endif

/**
 * @file BalanceRun.h
 * @brief 倒立走行
 * @author 川角 一天
 * @date 2019/07/29
 */

#ifndef __FORWARD_MOVING_H__
#define __FORWARD_MOVING_H__

#include "Motion.h"

/**
 * @brief 倒立走行クラス
 * @author 川角 一天
 * @date 2019/07/29
 */
class BalanceRun : public Motion
{
public:
	/**
	 * @brief コンストラクタ
	 * @author 川角 一天
	 * @date 2019/07/29
	 */
	BalanceRun();

	/**
	 * @brief デストラクタ
	 * @author 川角 一天
	 * @date 2019/07/29
	 */
	~BalanceRun();

	/**
	 * @brief 動作実行（速度指令）
	 * @author 川角 一天
	 * @date 2019/07/29
	 * @param [in] param 速度指令
	 * @return Motion実行結果
	 */
	MotionResult execute(SpeedCommand& param) override;

private:
	/**
	 * @brief 直近のPWM値に応じてエンコーダー値にバックラッシュ分の値を追加
	 * @author 川角 一天
	 * @date 2019/07/29
	 * @param [in] lenc 左モーターエンコーダー値
	 * @param [in] renc 右モーターエンコーダー値
	 */
	void cancelBacklash(int32_t& lenc, int32_t& renc);

	signed char m_pwm_L;		//! 左モーター前回PWM値
	signed char m_pwm_R;		//! 右モーター前回PWM値
};
#endif
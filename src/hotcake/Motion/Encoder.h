/**
 * @file Encoder.h
 * @brief エンコーダ値を取得管理
 * @author 平井 景
 * @date 2019/06/26
 */

#ifndef __ENCODER_H__
#define __ENCODER_H__

#include <ev3api.h>
#include <Motor.h>

using namespace ev3api;

/**
 * @brief エンコーダ値管理クラス
 * @author 平井 景
 * @date 2019/06/26
 */
class Encoder
{
public:
	/**
	 * @brief シングルトンなインスタンスを取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return Encoderクラスインスタンス
	 */
	static Encoder& getInstance();

	/**
	 * @brief エンコーダ値取得更新
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	void update();

	/**
	 * @brief エンコーダ値リセット
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	void reset();

	/**
	 * @brief 左モータインスタンス取得
	 * @author 平井 景
	 * @date 2019/08/06
	 * @return 左モータインスタンス
	 */
	Motor* getLeftMotor();

	/**
	 * @brief 右モータインスタンス取得
	 * @author 平井 景
	 * @date 2019/08/06
	 * @return 右モータインスタンス
	 */
	Motor* getRightMotor();

	/**
	 * @brief 尻尾モータインスタンス取得
	 * @author 平井 景
	 * @date 2019/08/06
	 * @return 尻尾モータインスタンス
	 */
	Motor* getTailMotor();

	/**
	 * @brief 左モータエンコーダ値取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return 左モータエンコーダ値
	 */
	int32_t getLeftCounts();

	/**
	 * @brief 右モータエンコーダ値取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return 右モータエンコーダ値
	 */
	int32_t getRightCounts();

	/**
	 * @brief 尻尾モータエンコーダ値取得
	 * @author 平井 景
	 * @date 2019/06/26
	 * @return 尻尾モータエンコーダ値
	 */
	int32_t getTailCounts();

private:
	/**
	 * @brief コンストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	Encoder();

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	~Encoder();

	/**
	 * @brief コピーコンストラクタ
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Encoder( const Encoder& );

	/**
	 * @brief コピー代入演算子
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Encoder& operator=( const Encoder& );

	Motor* m_leftMotor;		//! 左モータ
	Motor* m_rightMotor;	//! 右モータ
	Motor* m_tailMotor;		//! 尻尾モータ
	int32_t m_leftCounts;	//! 左モータエンコーダ値
	int32_t m_rightCounts;	//! 右モータエンコーダ値
	int32_t m_tailCounts;	//! 尻尾モータエンコーダ値
};

#endif // __ENCODER_H__

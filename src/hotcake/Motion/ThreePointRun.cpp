
/**
 * @file ThreePointRun.cpp
 * @brief 三点走行
 * @author 平井 景
 * @date 2019/08/15
 */

#include "System/Log.h"
#include "ThreePointRun.h"

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/08/15
 */
ThreePointRun::ThreePointRun()
{
}

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/08/15
 * @param [in] pidGain PIDゲイン値
 */
ThreePointRun::ThreePointRun(const PIDGain& pidGain) :
	Motion(pidGain)
{
}

/**
 * @brief デストラクタ
 * @author 濱津 信康
 * @date 2019/08/17
 */
ThreePointRun::~ThreePointRun()
{
}

/**
 * @brief 動作実行（速度指令）
 * @author 平井 景
 * @date 2019/08/15
 * @param [in] param 速度指令
 * @return Motion実行結果
 */
MotionResult ThreePointRun::execute(SpeedCommand& param)
{
	return MotionResult_Error;
}

/**
 * @brief 動作実行（位置指令）
 * @author 平井 景
 * @date 2019/08/15
 * @param [in] param 位置指令
 * @return Motion実行結果
 */
MotionResult ThreePointRun::execute(PositionCommand& param)
{
	return MotionResult_Error;
}

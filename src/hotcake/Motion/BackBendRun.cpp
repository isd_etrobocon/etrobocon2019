
/**
 * @file BackBendRun.cpp
 * @brief 後傾走行
 * @author 濱津 信康
 * @date 2019/08/13
 */

#include "BackBendRun.h"
#include "Odometer.h"
#include "System/Config.h"
#include "System/Log.h"

static const int SLOW_DOWN_FORWARD = 20;							//! 徐行
static const float GAIN_STEERING_KP = 10.0f;	//! 操舵角Pゲイン値
static const float GAIN_STEERING_KI = 0.0f;	//! 操舵角Iゲイン値
static const float GAIN_STEERING_KD = 0.0f;	//! 操舵角Dゲイン値

static const PIDGain PID_GAIN(GAIN_STEERING_KP, GAIN_STEERING_KI, GAIN_STEERING_KD);	// 操舵角PIDゲイン

/**
 * @brief コンストラクタ
 * @author 濱津 信康
 * @date 2019/08/13
 */
BackBendRun::BackBendRun() : 
	ThreePointRun(PID_GAIN)
{
}

/**
 * @brief デストラクタ
 * @author 濱津 信康
 * @date 2019/08/13
 */
BackBendRun::~BackBendRun()
{
}

/**
 * @brief 動作実行（速度指令）
 * @author 濱津 信康
 * @date 2019/08/18
 * @param [in] param 速度指令
 * @return Motion実行結果
 */
MotionResult BackBendRun::execute(SpeedCommand& param)
{
	uint8_t targetReflect = Config::getInstance().getBackbendRunTargetReflect();
	float steeringAngle = calculateSteeringPID(targetReflect, param.reflect);
	m_turnRatio = static_cast<int>(steeringAngle);

	if(0 == param.forward) {
		m_leftMotor->stop();
		m_rightMotor->stop();
	}
	else {
		m_steering->setPower(param.forward, static_cast<int>(steeringAngle));
	}

	return MotionResult_Continue;
}

/**
 * @brief 動作実行（位置指令）
 * @author 濱津 信康
 * @date 2019/08/13
 * @param [in] param 位置指令
 * @return Motion実行結果
 */
MotionResult BackBendRun::execute(PositionCommand& param)
{
   	int32_t totalDistance = Odometer::getInstance().getTotalDistance();

	MotionResult motionResult = MotionResult_Continue;
	if(param.mm1000times <= totalDistance) {
		m_leftMotor->stop();
		m_rightMotor->stop();
		motionResult = MotionResult_Done;
	}
	else {
		uint8_t targetReflect = Config::getInstance().getBackbendRunTargetReflect();
		float steeringAngle = calculateSteeringPID(targetReflect, param.reflect);
		m_turnRatio = static_cast<int>(steeringAngle);

		m_steering->setPower(SLOW_DOWN_FORWARD, static_cast<int>(steeringAngle));
	}

	return motionResult;
}

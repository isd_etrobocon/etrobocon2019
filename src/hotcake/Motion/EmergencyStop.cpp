
/**
 * @file EmergencyStop.cpp
 * @brief 緊急停止
 * @author 平井 景
 * @date 2019/08/04
 */

#include "EmergencyStop.h"
#include "System/Log.h"

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/08/04
 */
EmergencyStop::EmergencyStop()
{
}

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/08/04
 * @param [in] pidGain 操舵PID制御のゲイン値
 */
EmergencyStop::~EmergencyStop()
{
}

/**
 * @brief 動作実行
 * @author 平井 景
 * @date 2019/08/04
 * @return Motion実行結果
 */
MotionResult EmergencyStop::execute()
{
	ev3_speaker_play_tone( NOTE_C4, 1000 );

	m_leftMotor->stop();
	m_rightMotor->stop();

	return MotionResult_Done;
}

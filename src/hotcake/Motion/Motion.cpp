
/**
 * @file Motion.cpp
 * @brief 走行体動作
 * @author 川角 一天
 * @date 2019/07/29
 */

#include "System/Log.h"
#include "Encoder.h"
#include "Motion.h"

static const float DALTA_T = 0.004f;			//! 制御周期(4ms)
static const float TURN_MAX_VALUE = 100.0f;		//! 操舵値最大値
static const float TURN_MIN_VALUE = -100.0f;	//! 操舵値最小値

Steering* Motion::m_steering = NULL;

/**
 * @brief コンストラクタ
 * @author 川角 一天
 * @date 2019/07/29
 */
Motion::Motion() :
	m_turnRatio(0),
	m_steeringDifferentialDiff(0),
	m_steeringTotalIntegration(0),
	m_isReverse(false)
{
	m_leftMotor = Encoder::getInstance().getLeftMotor();
	m_rightMotor = Encoder::getInstance().getRightMotor();
	m_tailMotor = Encoder::getInstance().getTailMotor();
	if( NULL == m_steering ) {
		m_steering = new Steering( *m_leftMotor, *m_rightMotor );
		if( NULL == m_steering ){
			DEBUG_LOG
		}
	}
}

/**
 * @brief コンストラクタ
 * @author 川角 一天
 * @date 2019/07/29
 * @param [in] pidGain 操舵PID制御のゲイン値
 */
Motion::Motion(const PIDGain& pidGain) :
	Motion()
{
	m_pidGain.Kp = pidGain.Kp;
	m_pidGain.Ki = pidGain.Ki;
	m_pidGain.Kd = pidGain.Kd;
}

/**
 * @brief デストラクタ
 * @author 平井 景
 * @date 2019/08/06
 */
Motion::~Motion()
{
}

/**
 * @brief 動作実行
 * @author 川角 一天
 * @date 2019/07/29
 * @return Motion実行結果
 */
MotionResult Motion::execute()
{
	DEBUG_LOG
	return MotionResult_Error;
}

/**
 * @brief 動作実行（速度指令）
 * @author 川角 一天
 * @date 2019/07/29
 * @param [in] param 速度指令
 * @return Motion実行結果
 */
MotionResult Motion::execute(SpeedCommand& param)
{
	DEBUG_LOG
	return MotionResult_Error;
}

/**
 * @brief 動作実行（位置指令）
 * @author 平井 景
 * @date 2019/08/02
 * @param [in] param 位置指令
 * @return Motion実行結果
 */
MotionResult Motion::execute(PositionCommand& param)
{
	DEBUG_LOG
	return MotionResult_Error;
}

/**
 * @brief 動作実行（角度指令）
 * @author 平井 景
 * @date 2019/08/02
 * @param [in] param 角度指令
 * @return Motion実行結果
 */
MotionResult Motion::execute(AngleCommand& param)
{
	DEBUG_LOG
	return MotionResult_Error;
}

/**
 * @brief 操舵角PIDパラメータのリセット
 * @author 平井 景
 * @date 2019/08/17
 */
void Motion::resetSteeringPIDParam()
{
	m_steeringDifferentialDiff = 0;
	m_steeringTotalIntegration = 0;
}

/**
 * @brief 走行方向の反転
 * @author 平井 景
 * @date 2019/08/19
 */
void Motion::reverseDirection()
{
	m_isReverse = !m_isReverse;
}

/**
 * @brief 操舵方向取得
 * @author 平井 景
 * @date 2019/08/31
 * @return 操舵方向(-100〜100)
 */
int Motion::getTurn()
{
	return m_turnRatio;
}

/**
 * @brief 操舵値PID制御
 * @author 川角 一天
 * @date 2019/07/29
 * @param [in] targetReflect 輝度目標値
 * @param [in] nowReflect 輝度現在値
 * @return 操舵値
 */
float Motion::calculateSteeringPID(uint8_t targetReflect, uint8_t nowReflect)
{
	float turn = 0.0f;
	float proportional = 0.0f;
	float differential = 0.0f;
	float integration = 0.0f;
	int16_t diff_old = 0;
	int16_t diffReflect = 0;
	int16_t courseOP = 1;
	Config& config = Config::getInstance();
	Course course = config.getCourse();

	// 差分
	diffReflect = static_cast<int16_t>(targetReflect) - static_cast<int16_t>(nowReflect);

	// P制御（比例）
	proportional = static_cast<float>(diffReflect) * m_pidGain.Kp;

	// I制御（積分）
	m_steeringTotalIntegration += diffReflect * DALTA_T;

	integration = static_cast<float>(m_steeringTotalIntegration) * m_pidGain.Ki;

	// D制御（微分）
	diff_old = m_steeringDifferentialDiff;
	m_steeringDifferentialDiff = diffReflect;
	differential = static_cast<float>(m_steeringDifferentialDiff - diff_old) * m_pidGain.Kd / DALTA_T;

	// 2019年コース
	// Lコース：左エッジ
	// Rコース：右エッジ
	if( course == Course_L ) {
		courseOP = -1;
	}

	// 逆走判定
	if( m_isReverse ) {
		courseOP *= -1;
	}

	turn = (proportional + integration + differential) * static_cast<float>(courseOP);

	if (TURN_MAX_VALUE < turn) {
		turn = TURN_MAX_VALUE;
	}
	else if(turn < TURN_MIN_VALUE) {
		turn = TURN_MIN_VALUE;
	}
	else {
		// do nothing
	}

	return turn;
}

/**
 * @file Encoder.cpp
 * @brief エンコーダ値を取得管理
 * @author 平井 景
 * @date 2019/06/26
 */

#include "Encoder.h"
#include "System/Log.h"

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
Encoder::Encoder() :
	m_leftCounts(0),
	m_rightCounts(0),
	m_tailCounts(0)
{
	m_leftMotor = new Motor( PORT_C );
	if( NULL == m_leftMotor ){
		DEBUG_LOG
	}
	m_rightMotor = new Motor( PORT_B );
	if( NULL == m_rightMotor ){
		DEBUG_LOG
	}
	m_tailMotor = new Motor( PORT_A );
	if( NULL == m_tailMotor ){
		DEBUG_LOG
	}
}

/**
 * @brief デストラクタ
 * @author 平井 景
 * @date 2019/06/26
 */
Encoder::~Encoder()
{
}

/**
 * @brief シングルトンなインスタンスを取得
 * @author 平井 景
 * @date 2019/06/26
 * @return Encoderクラスインスタンス
 */
Encoder& Encoder::getInstance()
{
	static Encoder m_instance;
	return m_instance;
}


/**
 * @brief エンコーダ値取得更新
 * @author 平井 景
 * @date 2019/06/26
 */
void Encoder::update()
{
	m_leftCounts = m_leftMotor->getCount();
	m_rightCounts = m_rightMotor->getCount();
	m_tailCounts = m_tailMotor->getCount();
}


/**
 * @brief エンコーダ値リセット
 * @author 平井 景
 * @date 2019/06/26
 */
void Encoder::reset()
{
	m_leftCounts = 0;
	m_rightCounts = 0;
	m_tailCounts = 0;
	m_leftMotor->reset();
	m_rightMotor->reset();
}

/**
 * @brief 左モータインスタンス取得
 * @author 平井 景
 * @date 2019/08/06
 * @return 左モータインスタンス
 */
Motor* Encoder::getLeftMotor()
{
	return m_leftMotor;
}

/**
 * @brief 右モータインスタンス取得
 * @author 平井 景
 * @date 2019/08/06
 * @return 右モータインスタンス
 */
Motor* Encoder::getRightMotor()
{
	return m_rightMotor;
}

/**
 * @brief 尻尾モータインスタンス取得
 * @author 平井 景
 * @date 2019/08/06
 * @return 尻尾モータインスタンス
 */
Motor* Encoder::getTailMotor()
{
	return m_tailMotor;
}

/**
 * @brief 左モータエンコーダ値取得
 * @author 平井 景
 * @date 2019/06/26
 * @return 左モータエンコーダ値
 */
int32_t Encoder::getLeftCounts()
{
	return m_leftCounts;
}

/**
 * @brief 右モータエンコーダ値取得
 * @author 平井 景
 * @date 2019/06/26
 * @return 右モータエンコーダ値
 */
int32_t Encoder::getRightCounts()
{
	return m_rightCounts;
}

/**
 * @brief 尻尾モータエンコーダ値取得
 * @author 平井 景
 * @date 2019/06/26
 * @return 尻尾モータエンコーダ値
 */
int32_t Encoder::getTailCounts()
{
	return m_tailCounts;
}


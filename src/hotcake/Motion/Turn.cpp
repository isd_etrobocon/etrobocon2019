/**
 * @file Turn.cpp
 * @brief 反転
 * @author 濱津 信康
 * @date 2019/08/15
 */

#include "Turn.h"
#include "Encoder.h"
#include "Odometer.h"
#include <cstdlib>

static const float GAIN_TURN_KP = 2.0f;		//!旋回偏差Pゲイン値
static const float GAIN_TURN_KI = 0.0f;		//!旋回偏差Iゲイン値
static const float GAIN_TURN_KD = 0.0f;		//!旋回偏差Dゲイン値
static const int32_t TREAD_WIDTH_MM = 177;		//!トレッド幅
static const int32_t INIT_VALUE = 0;			//!初期化
static const int32_t TARGET_DEVIATION = 0;	//! 旋回量偏差目標値
static const int32_t TURN_PWM = 10;			//! 旋回時左右モータPWM値（絶対値）


/**
 * @brief コンストラクタ
 * @author 濱津 信康
 * @date 2019/08/15
 */
Turn::Turn() :
	m_startLeftCounts(0),
	m_startRightCounts(0),
	m_preLeftCounts(0),
	m_preRightCounts(0),
	m_turnDifferentialDiff(0),
	m_turnTotalIntegration(0),
	m_startFlg(false)
{
}

/**
 * @brief デストラクタ
 * @author 濱津 信康
 * @date 2019/08/15
 */
Turn::~Turn()
{
}

/**
 * @brief 動作実行（位置指令）
 * @author 濱津 信康
 * @date 2019/08/13
 * @param [in] param 位置指令
 * @return Motion実行結果
 */
MotionResult Turn::execute(AngleCommand& param)
{
	int32_t nowLeftCounts = Encoder::getInstance().getLeftCounts();
	int32_t nowRightCounts = Encoder::getInstance().getRightCounts();

	if(!m_startFlg) {
		m_startLeftCounts = nowLeftCounts;
		m_startRightCounts = nowRightCounts;
		m_startFlg = true;
	}

	MotionResult motionResult = MotionResult_Continue;
	if(isCompleted(param.angle ,nowLeftCounts, nowRightCounts)) {
		m_leftMotor->stop();
		m_rightMotor->stop();

		m_startLeftCounts = INIT_VALUE;
		m_startRightCounts = INIT_VALUE;
		m_preLeftCounts = INIT_VALUE;
		m_preRightCounts = INIT_VALUE;
		m_turnDifferentialDiff = INIT_VALUE;
		m_turnTotalIntegration = INIT_VALUE;
		m_startFlg = false;

		motionResult = MotionResult_Done;
	}
	else {
		int leftPwm = 0;
		int rightPwm = 0;
		calculateTurnPID(nowLeftCounts, nowRightCounts, &leftPwm, &rightPwm);

		m_leftMotor->setPWM(leftPwm);
		m_rightMotor->setPWM(rightPwm);
	}

	return motionResult;
}

/**
 * @brief 旋回角度判定
 * @author 濱津 信康
 * @date 2019/08/15
 * @param [in] targetAngle 角度目標値
 * @param [in] leftCounts 左モータエンコーダ値
 * @param [in] rightCounts 右モータエンコーダ値
 * @return 真:180度以上
 */
bool Turn::isCompleted(int targetAngle, int32_t leftCounts, int32_t rightCounts)
{
	bool ret = false;

	// ①指定角度その場旋回するためのモータ回転数x1000(周)
	uint32_t motorRotationNum1000times = static_cast<uint32_t>(((static_cast<float>(TREAD_WIDTH_MM) / static_cast<float>(WHEEL_DIRMETER)) / (360.0f / static_cast<float>(targetAngle))) * 1000.0f);

	// ②左右モータ移動量平均値x1000
	// ((絶対値(左エンコーダ値 - 開始左エンコーダ値) + 絶対値(右エンコーダ値 - 開始右エンコーダ値)) / 2) * 1000
	uint32_t bothMotorMoveAverage1000times = static_cast<uint32_t>((static_cast<float>(std::abs(leftCounts - m_startLeftCounts) + std::abs(rightCounts - m_startRightCounts)) / 2.0f) * 1000.0f);

	// (① * 360度) <= ②
	if((motorRotationNum1000times * 360) <= bothMotorMoveAverage1000times) {
		ret = true;
	}

	return ret;
}

/**
 * @brief 旋回偏差PID制御
 * @author 濱津 信康
 * @date 2019/08/15
 * @param [in] leftCounts 左モータエンコーダ値
 * @param [in] rightCounts 右モータエンコーダ値
 * @param [in] leftPwm 
 * @param [in] rightPwm 
 */
void Turn::calculateTurnPID(int32_t leftCounts, int32_t rightCounts, int* leftPwm, int* rightPwm)
{
	int controlAmount = 0;
	float proportional = 0.0f;
	float differential = 0.0f;
	float integration = 0.0f;
	int32_t diff_old = 0;
	int32_t diffDeviation = 0;

	// 旋回量偏差
	int32_t turnDeviation = (leftCounts = m_preLeftCounts) + (rightCounts = m_preRightCounts);
	m_preLeftCounts = leftCounts;
	m_preRightCounts = rightCounts;
	// 旋回量偏差目標値との差分
	diffDeviation = TARGET_DEVIATION - turnDeviation;
	
	// P制御（比例）
	proportional = static_cast<float>(diffDeviation) * m_pidGain.Kp;
	
	// I制御（積分）
	m_turnTotalIntegration += diffDeviation;
	integration = static_cast<float>(m_turnTotalIntegration) * m_pidGain.Ki;
	
	// D制御（微分）
	diff_old = m_turnDifferentialDiff;
	m_turnDifferentialDiff = diffDeviation;
	differential = static_cast<float>(m_turnDifferentialDiff - diff_old) * m_pidGain.Kd;

	// 制御量
	controlAmount = static_cast<int>(proportional + integration + differential + 0.5f);
	
	//左モータは正回転、右モータは逆回転とする
	*leftPwm = TURN_PWM + controlAmount;
	*rightPwm = (TURN_PWM * -1) + controlAmount;
}

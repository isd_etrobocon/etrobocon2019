/**
 * @file BackBendRun.h
 * @brief 後傾走行
 * @author 濱津 信康
 * @date 2019/08/13
 */

#ifndef __BACK_BEND_RUN_H__
#define __BACK_BEND_RUN_H__

#include "ThreePointRun.h"

/**
 * @file BackBendRun.h
 * @brief 後傾走行クラス
 * @author 濱津 信康
 * @date 2019/08/13
 */
class BackBendRun : public ThreePointRun
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津 信康
	 * @date 2019/08/13
	 */
	BackBendRun();

	/**
	 * @brief デストラクタ
	 * @author 濱津 信康
	 * @date 2019/08/13
	 */
	~BackBendRun();

	/**
	 * @brief 動作実行（速度指令）
	 * @author 濱津 信康
	 * @date 2019/08/18
	 * @param [in] param 速度指令
	 * @return Motion実行結果
	 */
	MotionResult execute(SpeedCommand& param) override;

	/**
	 * @brief 動作実行（位置指令）
	 * @author 濱津 信康
	 * @date 2019/08/13
	 * @param [in] param 位置指令
	 * @return Motion実行結果
	 */
	MotionResult execute(PositionCommand& param) override;
};
#endif

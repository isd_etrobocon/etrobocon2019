/**
 * @file ThreePointRun.h
 * @brief 三点走行
 * @author 多岐 龍星
 * @date 2019/08/03
 */

#ifndef __THREE_POINT_RUN_H__
#define __THREE_POINT_RUN_H__

#include "Motion.h"

/**
 * @brief 三点走行クラス
 * @author 多岐 龍星
 * @date 2019/08/03
 */
class ThreePointRun : public Motion
{
public:
	/**
	 * @brief コンストラクタ
	 * @author 多岐 龍星
	 * @date 2019/08/03
	 */
	ThreePointRun();

	/**
	 * @brief コンストラクタ
	 * @author 多岐 龍星
	 * @date 2019/08/03
	 * @param [in] pidGain PIDゲイン値
	 */
	ThreePointRun(const PIDGain& pidGain);

	/**
	 * @brief デストラクタ
	 * @author 多岐 龍星
	 * @date 2019/08/03
	 */
	virtual ~ThreePointRun() = 0;

	/**
	 * @brief 動作実行（速度指令）
	 * @author 多岐 龍星
	 * @date 2019/08/03
	 * @param [in] param 速度指令
	 * @return Motion実行結果
	 */
	virtual MotionResult execute(SpeedCommand& param) override;

	/**
	 * @brief 動作実行（位置指令）
	 * @author 多岐 龍星
	 * @date 2019/08/03
	 * @param [in] param 位置指令
	 * @return Motion実行結果
	 */
	virtual MotionResult execute(PositionCommand& param) override;
};
#endif

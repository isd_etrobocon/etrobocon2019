/**
 * @file TailMove.h
 * @brief 尻尾動作
 * @author 平井 景
 * @date 2019/06/26
 */

#ifndef __TAILLOWERING_H__
#define __TAILLOWERING_H__

#include "Motion.h"

/**
 * @brief 尻尾動作クラス
 * @author 平井 景
 * @date 2019/06/26
 */
class TailMove : public Motion
{
public:
	/**
	 * @brief コンストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	TailMove();

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/06/26
	 */
	~TailMove();

	/**
	 * @brief 動作実行
	 * @author 平井 景
	 * @date 2019/06/26
	 * @param [in] param 角度指令
	 * @return Motion実行結果
	 */
	MotionResult execute( AngleCommand& param ) override;

	/**
	 * @brief ブレーキロック
	 * @author 平井 景
	 * @date 2019/08/25
	 */
	void lock();

	/**
	 * @brief ブレーキロック解除
	 * @author 平井 景
	 * @date 2019/08/25
	 */
	void unlock();

private:
	bool m_isLock;
};

#endif // __TAILLOWERING_H__

/**
 * @file Turn.h
 * @brief 反転
 * @author 濱津 信康
 * @date 2019/08/14
 */

#ifndef __TURN_H__
#define __TURN_H__

#include "ThreePointRun.h"

/**
 * @file Turn.h
 * @brief 反転クラス
 * @author 濱津 信康
 * @date 2019/08/15
 */
class Turn : public Motion
{
public:

	/**
	 * @brief コンストラクタ
	 * @author 濱津 信康
	 * @date 2019/08/15
	 */
	Turn();

	/**
	 * @brief デストラクタ
	 * @author 濱津 信康
	 * @date 2019/08/15
	 */
	~Turn();

	/**
	 * @brief 動作実行（位置指令）
	 * @author 濱津 信康
	 * @date 2019/08/15
	 * @param [in] param 位置指令
	 * @return Motion実行結果
	 */
	MotionResult execute(AngleCommand& param) override;

private:

	/**
	 * @brief 旋回角度判定
	 * @author 濱津 信康
	 * @date 2019/08/15
	 * @param [in] targetAngle 角度目標値
	 * @param [in] leftCounts 左モータエンコーダ値
	 * @param [in] rightCounts 右モータエンコーダ値
	 * @return 真:180度以上
	 */
	bool isCompleted(int targetAngle, int32_t leftCounts, int32_t rightCounts);

	/**
	 * @brief 旋回偏差PID制御
	 * @author 濱津 信康
	 * @date 2019/08/15
	 * @param [in] leftCounts 左モータエンコーダ値
	 * @param [in] rightCounts 右モータエンコーダ値
 	 * @param [in] leftPwm 
	 * @param [in] rightPwm 
	 */
	void calculateTurnPID(int32_t leftCounts, int32_t rightCounts, int* leftPwm, int* rightPwm);

	int32_t m_startLeftCounts;		//! 開始左モータエンコーダ値
	int32_t m_startRightCounts;		//! 開始右モータエンコーダ値
	int32_t m_preLeftCounts;		//! 前回左モータエンコーダ値
	int32_t m_preRightCounts;		//! 前回右モータエンコーダ値
	int16_t m_turnDifferentialDiff;	//! 操舵PID微分値差分
	int32_t m_turnTotalIntegration;	//! 操舵PID積分値合計
	bool m_startFlg;				//! 開始フラグ
};
#endif

/**
 * @file Odometer.cpp
 * @brief 距離計
 * @author 平井 景
 * @date 2019/07/20
 */

#include <cmath>
#include <ev3api.h>
#include "Odometer.h"
#include "Encoder.h"

static const int32_t MMx1000_BY_ANGLE = WHEEL_CIRCUMFERENCE_MM * 1000 / 360;		// 1度あたりのタイヤ移動量(mm)*1000
static const int32_t ANGLE_BY_COUNT = 1;											// エンコーダ1カウントあたりの角度

/**
 * @brief コンストラクタ
 * @author 平井 景
 * @date 2019/07/20
 */
Odometer::Odometer() :
	m_remainderDistance(0),
	m_totalDistance(0),
	m_lastLeftCounts(0),
	m_lastRightCounts(0)
{
}

/**
 * @brief デストラクタ
 * @author 平井 景
 * @date 2019/07/20
 */
Odometer::~Odometer()
{
}

/**
 * @brief シングルトンなインスタンスを取得
 * @author 平井 景
 * @date 2019/07/20
 * @return Odometerクラスインスタンス
 */
Odometer& Odometer::getInstance()
{
	static Odometer m_instance;
	return m_instance;
}

/**
 * @brief 距離計更新（4ms周期更新）
 * @author 平井 景
 * @date 2019/07/20
 */
void Odometer::update()
{
	Encoder& encoder = Encoder::getInstance();
	encoder.update();

	int32_t nowLeftCounts = encoder.getLeftCounts();
	int32_t nowRightCounts = encoder.getRightCounts();

	int32_t leftWheelDistance = ( nowLeftCounts - m_lastLeftCounts ) * ANGLE_BY_COUNT * MMx1000_BY_ANGLE;
	int32_t rightWheelDistance = ( nowRightCounts - m_lastRightCounts ) * ANGLE_BY_COUNT * MMx1000_BY_ANGLE;

	// 前周期での移動距離計算の剰余を今周期の計算で足しこむことにより、誤差の拡大を防止
	int32_t cycleDistance = (leftWheelDistance + rightWheelDistance + m_remainderDistance) / 2;
	m_totalDistance += cycleDistance;

	m_remainderDistance = (leftWheelDistance + rightWheelDistance) % 2;

	m_lastLeftCounts = nowLeftCounts;
	m_lastRightCounts = nowRightCounts;
}

/**
 * @brief 距離計リセット
 * @author 平井 景
 * @date 2019/07/20
 */
void Odometer::reset()
{
	m_remainderDistance = 0;
	m_totalDistance = 0;
	m_lastLeftCounts = 0;
	m_lastRightCounts = 0;
}

/**
 * @brief 走行距離取得
 * @author 平井 景
 * @date 2019/07/20
 * @return 走行距離（単位はmm*1000)
 */
int32_t Odometer::getTotalDistance()
{
	return m_totalDistance;
}

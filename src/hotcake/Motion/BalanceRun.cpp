
/**
 * @file BalanceRun.cpp
 * @brief 倒立走行
 * @author 川角 一天
 * @date 2019/07/29
 */

#include "BalanceRun.h"
#include "Encoder.h"
#include "System/Log.h"

static const int32_t GYRO_OFFSET = 0;			//! ジャイロセンサオフセット値(角速度0[deg/sec]時)
static const float GAIN_STEERING_KP = 1.58f;	//! 操舵角Pゲイン値
static const float GAIN_STEERING_KI = 0.4f;		//! 操舵角Iゲイン値
static const float GAIN_STEERING_KD = 0.065f;	//! 操舵角Dゲイン値
// static const float GAIN_STEERING_KP = 1.42f;	//! 操舵角Pゲイン値
// static const float GAIN_STEERING_KI = 0.4f;		//! 操舵角Iゲイン値
// static const float GAIN_STEERING_KD = 0.0592f;	//! 操舵角Dゲイン値

static const PIDGain PID_GAIN(GAIN_STEERING_KP, GAIN_STEERING_KI, GAIN_STEERING_KD);	// 操舵角PIDゲイン

/**
 * @brief コンストラクタ
 * @author 川角 一天
 * @date 2019/07/29
 */
BalanceRun::BalanceRun() :
	Motion(PID_GAIN),
	m_pwm_L(0),
	m_pwm_R(0)
{
	balance_init();
}

/**
 * @brief デストラクタ
 * @author 川角 一天
 * @date 2019/07/29
 */
BalanceRun::~BalanceRun()
{
}

/**
 * @brief 動作実行（速度指令）
 * @author 川角 一天
 * @date 2019/07/29
 * @param [in] param 速度指令
 * @return Motion実行結果
 */
MotionResult BalanceRun::execute(SpeedCommand& param)
{
	float turn = 0.0f;
	int32_t leftCounts = 0;
	int32_t rightCounts = 0;
	uint8_t targetReflect = 0;
	Config& config = Config::getInstance();
	Encoder& encoder = Encoder::getInstance();;

	if(param.forward <= 100) {
		m_pidGain.Kp = 1.42f;
		m_pidGain.Ki = 0.4f;
		m_pidGain.Kd = 0.0592f;
	}

	targetReflect = config.getBalanceRunTargetReflect();
	turn = calculateSteeringPID(targetReflect, param.reflect);
	m_turnRatio = static_cast<int>(turn);

	// 倒立振子制御API に渡すパラメーターを取得する
	leftCounts = encoder.getLeftCounts();
	rightCounts = encoder.getRightCounts();

	// バックラッシュキャンセル
	cancelBacklash(leftCounts, rightCounts);

	// 倒立振子制御APIを呼び出し、倒立走行するための
	// 左右モーター出力値を得る
	balance_control(
		static_cast<float>(param.forward),// 前進命令
		turn,
		static_cast<float>(param.angerVelocity),
		static_cast<float>(GYRO_OFFSET),
		static_cast<float>(leftCounts),
		static_cast<float>(rightCounts),
		static_cast<float>(param.mV),
		&m_pwm_L,
		&m_pwm_R
		);

	m_leftMotor->setPWM( static_cast<int>(m_pwm_L) );
	m_rightMotor->setPWM( static_cast<int>(m_pwm_R) );

	return MotionResult_Continue;
}

/**
 * @brief 直近のPWM値に応じてエンコーダー値にバックラッシュ分の値を追加
 * @author 川角 一天
 * @date 2019/07/29
 * @param [in] lenc 左モーターエンコーダー値
 * @param [in] renc 右モーターエンコーダー値
 */
void BalanceRun::cancelBacklash(int32_t& lenc, int32_t& renc)
{
	const int BACKLASHHALF = 4;   // バックラッシュの半分[deg]

	if(m_pwm_L < 0) {
		lenc += BACKLASHHALF;
	}
	else if(m_pwm_L > 0) {
		lenc -= BACKLASHHALF;
	}
	else {
		// do nothing
	}

	if(m_pwm_R < 0) {
		renc += BACKLASHHALF;
	}
	else if(m_pwm_R > 0) {
		renc -= BACKLASHHALF;
	}
	else {
		// do nothing
	}
}

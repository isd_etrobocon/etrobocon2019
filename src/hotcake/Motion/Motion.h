/**
 * @file Motion.h
 * @brief 走行体動作
 * @author 川角 一天
 * @date 2019/07/29
 */

#ifndef __MOTION_H__
#define __MOTION_H__

#include <ev3api.h>
#include <Motor.h>
#include <Steering.h>
#include "balancer.h"
#include "System/Config.h"

using namespace ev3api;

//! Motion実行結果
enum MotionResult
{
	MotionResult_Done = 0,	//! 指示動作完了
	MotionResult_Continue,	//! 指示動作継続
	MotionResult_Error		//! エラー発生
};

//! 速度指令
struct SpeedCommand
{
	int forward;			//! 速度指令（正：前進、負：後退）
	uint8_t reflect;		//! 現在輝度
	int16_t angerVelocity;	//! 現在角速度
	int mV;					//! 現在電圧値

	SpeedCommand() :
		forward(0),
		reflect(0),
		angerVelocity(0),
		mV(0)
		{}
};

//! 位置指令
struct PositionCommand
{
	int32_t mm1000times;	//! 位置指令（単位：mm*1000 正：前進、負：後退）
	uint8_t reflect;		//! 現在輝度
	int16_t angerVelocity;	//! 現在角速度
	int mV;					//! 現在電圧値

	PositionCommand() :
		mm1000times(0),
		reflect(0),
		angerVelocity(0),
		mV(0)
		{}
};

//! 角度指令
struct AngleCommand
{
	int angle;				//! 角度指令
	uint8_t reflect;		//! 現在輝度
	int16_t angerVelocity;	//! 現在角速度
	int mV;					//! 現在電圧値

	AngleCommand() :
		angle(0),
		reflect(0),
		angerVelocity(0),
		mV(0)
		{}
};

//! PID制御のゲイン値
struct PIDGain
{
	float Kp;	//! 比例ゲイン値
	float Ki;	//! 微分ゲイン値
	float Kd;	//! 積分ゲイン値

	PIDGain() :
		Kp(0.0f),
		Ki(0.0f),
		Kd(0.0f)
	{}

	PIDGain(float kp, float ki, float kd) :
		Kp(kp),
		Ki(ki),
		Kd(kd)
	{}
};

/**
 * @brief 走行体動作基底クラス
 * @author 川角 一天
 * @date 2019/07/29
 */
class Motion
{
public:
	/**
	 * @brief コンストラクタ
	 * @author 川角 一天
	 * @date 2019/07/29
	 */
	Motion();

	/**
	 * @brief コンストラクタ
	 * @author 川角 一天
	 * @date 2019/07/29
	 * @param [in] pidGain 操舵PID制御のゲイン値
	 */
	Motion(const PIDGain& pidGain);

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/08/06
	 */
	virtual ~Motion() = 0;

	/**
	 * @brief 動作実行
	 * @author 川角 一天
	 * @date 2019/07/29
	 * @return Motion実行結果
	 */
	virtual MotionResult execute();

	/**
	 * @brief 動作実行（速度指令）
	 * @author 川角 一天
	 * @date 2019/07/29
	 * @param [in] param 速度指令
	 * @return Motion実行結果
	 */
	virtual MotionResult execute(SpeedCommand& param);

	/**
	 * @brief 動作実行（位置指令）
	 * @author 平井 景
	 * @date 2019/08/02
	 * @param [in] param 位置指令
	 * @return Motion実行結果
	 */
	virtual MotionResult execute(PositionCommand& param);

	/**
	 * @brief 動作実行（角度指令）
	 * @author 平井 景
	 * @date 2019/08/02
	 * @param [in] param 角度指令
	 * @return Motion実行結果
	 */
	virtual MotionResult execute(AngleCommand& param);

	/**
	 * @brief 操舵角PIDパラメータのリセット
	 * @author 平井 景
	 * @date 2019/08/17
	 */
	void resetSteeringPIDParam();

	/**
	 * @brief 走行方向の反転
	 * @author 平井 景
	 * @date 2019/08/19
	 */
	void reverseDirection();

	/**
	 * @brief 操舵方向取得
	 * @author 平井 景
	 * @date 2019/08/31
	 * @return 操舵方向(左：-100〜100：右)
	 */
	int getTurn();

protected:
	/**
	 * @brief 操舵値PID制御
	 * @author 川角 一天
	 * @date 2019/07/29
	 * @param [in] targetReflect 輝度目標値
	 * @param [in] nowReflect 輝度現在値
	 * @return 操舵値
	 */
	float calculateSteeringPID(uint8_t targetReflect, uint8_t nowReflect);

	Motor* m_leftMotor;				//! 左モータ
	Motor* m_rightMotor;			//! 右モータ
	Motor* m_tailMotor;				//! 尻尾モータ
	static Steering* m_steering;	//! ステアリング
	PIDGain m_pidGain;				//! PID操舵制御のゲイン値
	int m_turnRatio;						//! 操舵方向（記録用）

private:
	int16_t m_steeringDifferentialDiff;	//! 操舵PID微分値差分
	int32_t m_steeringTotalIntegration;	//! 操舵PID積分値合計
	bool m_isReverse;					//! 逆走判定
};

#endif

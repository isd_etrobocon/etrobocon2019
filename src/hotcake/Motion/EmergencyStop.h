/**
 * @file EmergencyStop.h
 * @brief 緊急停止
 * @author 平井 景
 * @date 2019/08/04
 */

#ifndef __EMERGENCY_STOP_H__
#define __EMERGENCY_STOP_H__

#include "Motion.h"

/**
 * @brief 倒立走行クラス
 * @author 平井 景
 * @date 2019/08/04
 */
class EmergencyStop : public Motion
{
public:
	/**
	 * @brief コンストラクタ
	 * @author 平井 景
	 * @date 2019/08/04
	 */
	EmergencyStop();

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/08/04
	 */
	~EmergencyStop();

	/**
	 * @brief 動作実行
	 * @author 平井 景
	 * @date 2019/08/04
	 * @return Motion実行結果
	 */
	MotionResult execute() override;
};
#endif
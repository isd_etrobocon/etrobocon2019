/**
 * @file Odometer.h
 * @brief 距離計
 * @author 平井 景
 * @date 2019/07/20
 */

#ifndef __ODOMETER_H__
#define __ODOMETER_H__

static const int32_t WHEEL_DIRMETER = 101;
static const int32_t WHEEL_CIRCUMFERENCE_MM = static_cast<int32_t>(static_cast<float>(WHEEL_DIRMETER) * 3.14);	// タイヤの円周(mm)

/**
 * @brief 距離計クラス
 * @author 平井 景
 * @date 2019/07/20
 */
class Odometer
{
public:
	/**
	 * @brief シングルトンなインスタンスを取得
	 * @author 平井 景
	 * @date 2019/07/20
	 * @return Odometerクラスインスタンス
	 */
	static Odometer& getInstance();

	/**
	 * @brief 距離計更新（4ms周期更新）
	 * @author 平井 景
	 * @date 2019/07/20
	 */
	void update();

	/**
	 * @brief 距離計リセット
	 * @author 平井 景
	 * @date 2019/07/20
	 */
	void reset();

	/**
	 * @brief 走行距離取得
	 * @author 平井 景
	 * @date 2019/07/20
	 * @return 走行距離（単位はmm*1000)
	 */
	int32_t getTotalDistance();

private:
	/**
	 * @brief コンストラクタ
	 * @author 平井 景
	 * @date 2019/07/20
	 */
	Odometer();

	/**
	 * @brief デストラクタ
	 * @author 平井 景
	 * @date 2019/07/20
	 */
	~Odometer();

	/**
	 * @brief コピーコンストラクタ
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Odometer( const Odometer& );

	/**
	 * @brief コピー代入演算子
	 * @author 平井 景
	 * @date 2019/08/02
	 * @note シングルトンコピー防止のためprivateかつ定義なし
	 */
	Odometer& operator=( const Odometer& );

	int32_t m_remainderDistance;	//! 前周期での平均計算の剰余
	int32_t m_totalDistance;		//! 走行距離
	int32_t m_lastLeftCounts;		//! 前回左モータエンコーダ値
	int32_t m_lastRightCounts;		//! 前回右モータエンコーダ値
};

#endif // __ODOMETER_H__

# Cファイル
APPL_COBJS += balancer.o \
			  balancer_param.o \

# CPPファイル
APPL_CXXOBJS += StageAction.o \
				InitializeAction.o \
				IdleAction.o \
				NormalRunAction.o \
				LookupGateAction.o \
				SeesawAction.o \
				ParkingAction.o \
				Bluetooth.o \
				Config.o \
				Log.o \
				Motion.o \
				BalanceRun.o \
				TailMove.o \
				Encoder.o \
				Odometer.o \
				EmergencyStop.o \
				ThreePointRun.o \
				TailRun.o \
				BackBendRun.o \
				Turn.o \

SRCLANG := c++

# Warning設定
CPPWARNINGS = -Wfloat-equal -Wswitch-default

# コンパイルオプション
COPTS += $(CPPWARNINGS)

ifdef CONFIG_EV3RT_APPLICATION

# Include libraries
include $(EV3RT_SDK_LIB_DIR)/libcpp-ev3/Makefile

endif

/**
* @file app.cpp
* @brief メインタスク
* @author 濱津
* @date 2019/06/30
*/
#include <ev3api.h>
#include <Clock.h>
#include "app.h"
#include "Stage/StageAction.h"
#include "Stage/InitializeAction.h"
#include "Stage/IdleAction.h"
#include "Stage/NormalRunAction.h"
#include "Stage/LookupGateAction.h"
#include "Stage/SeesawAction.h"
#include "Stage/ParkingAction.h"
#include "System/Log.h"

// [シングルトンビルドエラー回避]
// コンパイラで__sync_synchronize()が未定義となるため、
// 代替手段（アセンブラ）でメモリバリアを実装する。
extern "C" {
void __sync_synchronize()
{
	asm volatile("" ::: "memory");
}
}

/**
 * @brief メインタスク
 * @author 濱津
 * @date 2019/6/30
 * @param [in] exinf 未使用
 */
void mainTask(intptr_t exinf)
{
	Scenario::getInstance().scenarioLoop();
	ext_tsk();
}

/**
 * @brief コンストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
Scenario::Scenario() :
	m_stage(Stage_Initialize)
{
	for(int i = 0; i < Stage_MaxNum; i++) {
		m_stageActions[i] = NULL;
	}
}

/**
 * @brief デストラクタ
 * @author 濱津
 * @date 2019/6/30
 */
Scenario::~Scenario()
{

}

/**
 * @brief シングルトンなインスタンスを取得
 * @author 濱津
 * @date 2019/6/30
 * @return Scenarioクラスインスタンス
 */
Scenario& Scenario::getInstance()
{
	static Scenario m_instance;
	return m_instance;
}

/**
 * @brief シナリオループ
 * @author 濱津
 * @date 2019/6/30
 */
void Scenario::scenarioLoop()
{
	Clock clock;
	m_stageActions[Stage_Initialize] = new InitializeAction;
	m_stageActions[Stage_Idle] = new IdleAction;
	m_stageActions[Stage_NormalRun] = new NormalRunAction;
	m_stageActions[Stage_LookupGate] = new LookupGateAction;
	m_stageActions[Stage_Seasaw] = new SeesawAction;
	m_stageActions[Stage_Parking] = new ParkingAction;

	for( int i = 0; i < Stage_MaxNum; i++ ) {
		if( NULL == m_stageActions[i] ) {
			DEBUG_LOGF( "ptr is NULL!! index=[%d]", i )
			return;
		}
	}

	while (1) {
		uint32_t startMsec = clock.now();
		StageResult stageResult = m_stageActions[m_stage]->execute();

		if( StageResult_Completed == stageResult ) {
			transitStage();

			if( Stage_Parking < m_stage ) {
				DEBUG_LOG
				break;
			}
		}
		else if( StageResult_Error == stageResult ) {
			DEBUG_LOG
			break;
		}
		else {
			// StageResult_Continue.
		}

		uint32_t endMsec = clock.now();
		// 4msec順守チェック
		if( 4 < ( endMsec - startMsec ) ) {
			DEBUG_LOGF("4msec Over!! [%lu]", ( endMsec - startMsec ));
		}

		clock.sleep( 4 ); // 4msec周期起動
	}

	// 蓄積ログ吐き出し指示待ちのためにBluetoothタスク再開
	Bluetooth::getInstance().start();

	delete m_stageActions[Stage_Initialize];
	delete m_stageActions[Stage_Idle];
	delete m_stageActions[Stage_NormalRun];
	delete m_stageActions[Stage_LookupGate];
	delete m_stageActions[Stage_Seasaw];
	delete m_stageActions[Stage_Parking];
}

/**
 * @brief stage状態遷移
 * @author 濱津
 * @date 2019/6/30
 */
void Scenario::transitStage()
{
	++m_stage;
	DEBUG_LOG
}

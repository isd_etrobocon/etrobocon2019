#include <string.h>
#include <ev3api.h>
#include <Clock.h>
#include <ColorSensor.h>
#include "app.h"

#if defined(BUILD_MODULE)
#include "module_cfg.h"
#else
#include "kernel_cfg.h"
#endif

using namespace ev3api;

// シングルトンビルドエラー回避
// コンパイラで__sync_synchronize()が未定義であるため、
// 代替手段（アセンブラ）でメモリバリアを実装する。
extern "C" {
	void __sync_synchronize()
	{
	   asm volatile("" ::: "memory");
	}
}

static const uint8_t TAIL_ANGLE_INIT			= 0;
static const uint8_t TAIL_ANGLE_BACKBEND_RUN	= 65;
static const uint8_t TAIL_ANGLE_TAIL_RUN		= 75;
static const uint8_t TAIL_ANGLE_STAND_UP		= 90;
static const uint8_t BUTTON_PRESS_NUM_MAX		= 3;
static const uint8_t PWM_ABS_MAX        		= 60;	 /* 完全停止用モーター制御PWM絶対最大値 */
static uint8_t g_btnPressedNum = 0;

uint8_t convertRgbToReflect( rgb_raw_t& rgb );
uint8_t getTargetTailAngle( uint8_t btnPressedNum );
bool tail_control( signed int angle );

void main_task(intptr_t unused)
{
	ColorSensor color( PORT_3 );
	ev3_lcd_set_font( EV3_FONT_MEDIUM );

	int32_t xWidth = 0;
	int32_t yWidth = 0;
	ev3_font_get_size( EV3_FONT_MEDIUM, &xWidth, &yWidth );

	int mode = 0;

	bool ret = false;
	signed int targetAngle = 0;

	ev3_motor_config( EV3_PORT_A, LARGE_MOTOR );
	ev3_motor_reset_counts( EV3_PORT_A );
	
	ev3_sensor_config( EV3_PORT_1, TOUCH_SENSOR );

	g_btnPressedNum = 0;

	while (1)
	{
		int32_t x = 10;
		int32_t y = 10;
		Clock clock;

		ev3_lcd_fill_rect( 0, 0, EV3_LCD_WIDTH , EV3_LCD_HEIGHT, EV3_LCD_WHITE );

		if( ev3_button_is_pressed( LEFT_BUTTON ) ) {
			mode = 0;	// RGB
		}
		else if( ev3_button_is_pressed( RIGHT_BUTTON ) ) {
			mode = 1;	// Color enum
		}
		else if( ev3_button_is_pressed( UP_BUTTON ) ) {
			mode = 2;	// bright
		}
		else if( ev3_button_is_pressed( DOWN_BUTTON ) ) {
			mode = 3;	// bright
		}
		else if( ev3_touch_sensor_is_pressed( EV3_PORT_1 ) ) {
			mode = 4;	//tail move
		}else {
			//Do Nothing
		}

		char str[32] = {};

		switch( mode ) {
			case 0:
				{
					rgb_raw_t rgb = {};
					color.getRawColor( rgb );

					snprintf( str, sizeof(str)-1, "R = %u", rgb.r );
					ev3_lcd_draw_string( str, x, y );
					y = y + yWidth + 10;

					memset( str, 0, sizeof(str) );
					snprintf( str, sizeof(str)-1, "G = %u", rgb.g );
					ev3_lcd_draw_string( str, x, y );
					y = y + yWidth + 10;

					memset( str, 0, sizeof(str) );
					snprintf( str, sizeof(str)-1, "B = %u", rgb.b );
					ev3_lcd_draw_string( str, x, y );
				}
				break;

			case 1:
				{
					colorid_t colorid = color.getColorNumber();
					switch(colorid) {
						case COLOR_NONE:
							snprintf( str, sizeof(str)-1, "COLOR_NONE" );
							break;

						case COLOR_BLACK:
							snprintf( str, sizeof(str)-1, "COLOR_BLACK" );
							break;

						case COLOR_BLUE:
							snprintf( str, sizeof(str)-1, "COLOR_BLUE" );
							break;

						case COLOR_GREEN:
							snprintf( str, sizeof(str)-1, "COLOR_GREEN" );
							break;

						case COLOR_YELLOW:
							snprintf( str, sizeof(str)-1, "COLOR_YELLOW" );
							break;

						case COLOR_RED:
							snprintf( str, sizeof(str)-1, "COLOR_RED" );
							break;

						case COLOR_WHITE:
							snprintf( str, sizeof(str)-1, "COLOR_WHITE" );
							break;

						case COLOR_BROWN:
							snprintf( str, sizeof(str)-1, "COLOR_BROWN" );
							break;

						default:
							break;
					}
					ev3_lcd_draw_string( str, x, y );
				}
				break;

			case 2:
				{
					snprintf( str, sizeof(str)-1, "Reflect = %u", color.getBrightness() );
					ev3_lcd_draw_string( str, x, y );
				}
				break;

			case 3:
				{
					rgb_raw_t rgb = {};
					color.getRawColor( rgb );
					uint8_t reflect = convertRgbToReflect( rgb );

					snprintf( str, sizeof(str)-1, "Y = %u", reflect );
					ev3_lcd_draw_string( str, x, y );
				}
				break;

			case 4:
				{
					targetAngle = (signed int )getTargetTailAngle( g_btnPressedNum );

					snprintf( str, sizeof(str)-1, "Num = %d", g_btnPressedNum );
					ev3_lcd_draw_string( str, x, y );
					y = y + yWidth + 10;

					memset( str, 0, sizeof(str) );
					snprintf( str, sizeof(str)-1, "tAn = %d", targetAngle );
					ev3_lcd_draw_string( str, x, y );
					
					while(1)
					{
						ret = tail_control( targetAngle );
						if( true == ret ) {
							break;
						}
						clock.sleep(  4);
					}
					if ( BUTTON_PRESS_NUM_MAX <= g_btnPressedNum ) {
						g_btnPressedNum = 0;
					}
					else {
						g_btnPressedNum++;
					}
					mode = 0;
				}
				break;

			default:
				break;
		}
		clock.sleep(500);
	}
}

uint8_t convertRgbToReflect( rgb_raw_t& rgb )
{
	// グレースケール（≒輝度）へ変換
	// [RGB単純平均]
	// uint8_t reflect = static_cast<uint8_t>((((static_cast<float>(rgb.r + rgb.g + rgb.b) / 3.0f) / 256.0f) * 100.0f) + 0.5f);
	// [ITU-R Rec BT.601 規格]
	// uint8_t reflect = static_cast<uint8_t>(((((0.299f * static_cast<float>(rgb.r)) + (0.587f * static_cast<float>(rgb.g)) + (0.114f * static_cast<float>(rgb.b))) / 256.0f) * 100.0f) + 0.5f);
	// [CIE XYZのYへ変換(輝度保存変換)]
	uint8_t reflect = static_cast<uint8_t>(((((0.2126f * static_cast<float>(rgb.r)) + (0.7152f * static_cast<float>(rgb.g)) + (0.0722f * static_cast<float>(rgb.b))) / 256.0f) * 100.0f) + 0.5f);
	if( 100 < reflect ) {
		reflect = 100;
	}

	return reflect;
}

uint8_t getTargetTailAngle( uint8_t btnPressedNum )
{
	uint8_t result = 0;

	switch ( btnPressedNum )
	{
	case 0:
		{
			result = TAIL_ANGLE_INIT;
		}
		break;

	case 1:
		{
			result = TAIL_ANGLE_BACKBEND_RUN;
		}
		break;

	case 2:
		{
			result = TAIL_ANGLE_TAIL_RUN;
		}
		break;

	case 3:
		{
			result = TAIL_ANGLE_STAND_UP;
		}
		break;
	
	default:
		break;
	}
	return result;
}

bool tail_control( signed int angle )
{
    signed int pwm = (signed int)((angle - ev3_motor_get_counts(EV3_PORT_A)));

    if ( 0 == pwm ) {
        ev3_motor_stop( EV3_PORT_A, true );
		return true;
    }
    else {
        ev3_motor_set_power( EV3_PORT_A, pwm );
		return false;
    }
}

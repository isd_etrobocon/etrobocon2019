$(function() {

    $('#sendButton').on('click', function(event) {
	var url = 'ws://' + location.host + '/serial/';
	var ws = new WebSocket(url);
	ws.addEventListener('open', function (event) {
	    var codes = $('#sendText').val().split(/\n/);
	    $.each(codes,function(index,val){
		var msg = {"type":"text","value":val};
		ws.send(JSON.stringify(msg));
	    });
	    ws.close();
	});
	ws.addEventListener('close', function (event) {
            alert("Source Code Sent!");
	});
    });
    
 })

$(function () {
	const _cmdEcho = "Hello, hotcake!!";
	const _cmdGetSettings = "AllSettings";
	const _cmdStart = "Start";

	const _setCourseL = "Course : L";
	const _setCourseR = "Course : R";
	const _balanceRunTargetReflect = "BalanceRunTargetReflect : ";
	const _tailRunTargetReflect = "TailRunTargetReflect : ";
	const _backbendRunTargetReflect = "BackbendRunTargetReflect : ";

	const _cmdGetStoredLogs = "StoredLogs";

	disableSession();
	var ws;

	$('#connectButton').on('click', function (event) {
		var url = 'ws://' + location.host + '/serial/';
		ws = new WebSocket(url);
		ws.addEventListener('message', function (event) {
			var obj = JSON.parse(event.data);
			var obj = JSON.parse(event.data);
			var date = new Date();
			var hour = toDoubleDigits(date.getHours());
			var min = toDoubleDigits(date.getMinutes());
			var sec = toDoubleDigits(date.getSeconds());
			$('#recvText').val($('#recvText').val() + '\n[' + hour + ":" + min + ":" + sec + "] " + obj.value);
			// $('#recvText').scrollTop($('#recvText')[0].scrollHeight);
		});
		ws.addEventListener('open', function (event) {
			enableSession();
		});
		ws.addEventListener('close', function (event) {
			disableSession();
		});
	});

	$('#closeButton').on('click', function (event) {
		ws.close();
	});

	$('#echoButton').on('click', function (event) {
		var msg = { "type": "text", "value": _cmdEcho };
		ws.send(JSON.stringify(msg));
	});

	$('#getSettingsButton').on('click', function (event) {
		var msg = { "type": "text", "value": _cmdGetSettings };
		ws.send(JSON.stringify(msg));
	});

	$('#startButton').on('click', function (event) {
		var msg = { "type": "text", "value": _cmdStart };
		ws.send(JSON.stringify(msg));
	});

	$('#courseLButton').on('click', function (event) {
		var msg = { "type": "text", "value": _setCourseL };
		ws.send(JSON.stringify(msg));
	});

	$('#courseRButton').on('click', function (event) {
		var msg = { "type": "text", "value": _setCourseR };
		ws.send(JSON.stringify(msg));
	});

	$('#balanceRunTargetReflectButton').on('click', function (event) {
		var msg = { "type": "text", "value": _balanceRunTargetReflect + $('#balanceRunTargetReflectText').val() };
		ws.send(JSON.stringify(msg));
	});

	$('#tailRunTargetReflectButton').on('click', function (event) {
		var msg = { "type": "text", "value": _tailRunTargetReflect + $('#tailRunTargetReflectText').val() };
		ws.send(JSON.stringify(msg));
	});

	$('#backbendRunTargetReflectButton').on('click', function (event) {
		var msg = { "type": "text", "value": _backbendRunTargetReflect + $('#backbendRunTargetReflectText').val() };
		ws.send(JSON.stringify(msg));
	});

	$('#targetReflectButton').on('click', function (event) {
		var msg1 = { "type": "text", "value": _balanceRunTargetReflect + $('#balanceRunTargetReflectText').val() };
		ws.send(JSON.stringify(msg1));
		var msg2 = { "type": "text", "value": _tailRunTargetReflect + $('#tailRunTargetReflectText').val() };
		ws.send(JSON.stringify(msg2));
		var msg3 = { "type": "text", "value": _backbendRunTargetReflect + $('#backbendRunTargetReflectText').val() };
		ws.send(JSON.stringify(msg3));
	});

	$('#sendButton').on('click', function (event) {
		var msg = { "type": "text", "value": $('#sendText').val() };
		ws.send(JSON.stringify(msg));
	});

	$('#escButton').on('click', function (event) {
		var msg = { "type": "esc" };
		ws.send(JSON.stringify(msg));
	});

	$('#logClearButton').on('click', function (event) {
		$('#recvText').val("")
	});

	$('#logCopyButton').on('click', function (event) {
		var copyTarget = document.getElementById("recvText");
		copyTarget.select();
		document.execCommand("Copy");
	});

	$('#storedLogsButton').on('click', function (event) {
		var msg = { "type": "text", "value": _cmdGetStoredLogs };
		ws.send(JSON.stringify(msg));
	});

	function enableSession() {
		$('#connectButton').prop('disabled', true);
		$('#closeButton').prop('disabled', false);

		$('#echoButton').prop('disabled', false);
		$('#getSettingsButton').prop('disabled', false);
		$('#startButton').prop('disabled', false);

		$('#courseLButton').prop('disabled', false);
		$('#courseRButton').prop('disabled', false);

		$('#balanceRunTargetReflectText').prop('disabled', false);
		$('#balanceRunTargetReflectButton').prop('disabled', false);
		$('#tailRunTargetReflectText').prop('disabled', false);
		$('#tailRunTargetReflectButton').prop('disabled', false);
		$('#backbendRunTargetReflectText').prop('disabled', false);
		$('#backbendRunTargetReflectButton').prop('disabled', false);
		$('#targetReflectButton').prop('disabled', false);

		$('#escButton').prop('disabled', false);
		$('#sendText').prop('disabled', false);
		$('#sendButton').prop('disabled', false);

		$('#storedLogsButton').prop('disabled', false);
	}

	function disableSession() {
		$('#connectButton').prop('disabled', false);
		$('#closeButton').prop('disabled', true);

		$('#echoButton').prop('disabled', true);
		$('#getSettingsButton').prop('disabled', true);
		$('#startButton').prop('disabled', true);

		$('#courseLButton').prop('disabled', true);
		$('#courseRButton').prop('disabled', true);

		$('#balanceRunTargetReflectText').prop('disabled', true);
		$('#balanceRunTargetReflectButton').prop('disabled', true);
		$('#tailRunTargetReflectText').prop('disabled', true);
		$('#tailRunTargetReflectButton').prop('disabled', true);
		$('#backbendRunTargetReflectText').prop('disabled', true);
		$('#backbendRunTargetReflectButton').prop('disabled', true);
		$('#targetReflectButton').prop('disabled', true);

		$('#escButton').prop('disabled', true);
		$('#sendText').prop('disabled', true);
		$('#sendButton').prop('disabled', true);

		$('#storedLogsButton').prop('disabled', true);
	}

	function toDoubleDigits(num) {
		num += "";
		if (num.length === 1) {
			num = "0" + num;
		}
		return num;
	}
})

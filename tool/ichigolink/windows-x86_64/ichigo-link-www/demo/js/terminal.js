$(function() {

    disableSession();
    var ws;

    $('#connectButton').on('click', function(event) {
	var url = 'ws://' + location.host + '/serial/';
	ws = new WebSocket(url);
	ws.addEventListener('message', function (event) {
	    var obj = JSON.parse(event.data);
	    $('#recvText').val($('#recvText').val() + '\n' + obj.value);
	    $('#recvText').scrollTop($('#recvText')[0].scrollHeight);
	});
	ws.addEventListener('open', function (event) {
	    enableSession();
	});
	ws.addEventListener('close', function (event) {
	    disableSession();
	});
    });
    
    $('#sendButton').on('click', function(event) {
	var msg = {"type":"text","value":$('#sendText').val()};
	ws.send(JSON.stringify(msg));
    });
    
    $('#escButton').on('click', function(event) {
	var msg = {"type":"esc"};
	ws.send(JSON.stringify(msg));
    });

    function enableSession() {
	$('#connectButton').prop('disabled',true);
	
	$('#escButton').prop('disabled',false);
	$('#sendText').prop('disabled',false);
	$('#sendButton').prop('disabled',false);
	$('#recvText').prop('disabled',false);
    }
    
    function disableSession() {
	$('#connectButton').prop('disabled',false);
	
	$('#escButton').prop('disabled',true);
	$('#sendText').prop('disabled',true);
	$('#sendButton').prop('disabled',true);
	$('#recvText').prop('disabled',true);
    }
    
 })

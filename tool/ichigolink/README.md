# README


<!-- TOC -->

- [IchigoLinkについて](#ichigolinkについて)
	- [概要](#概要)
	- [できること](#できること)
	- [使用手順](#使用手順)
		- [各ボタンの説明](#各ボタンの説明)
			- [Commands](#commands)
			- [Settings](#settings)
			- [Free Format](#free-format)
			- [Logs](#logs)
	- [注意点](#注意点)

<!-- /TOC -->


## IchigoLinkについて

[公式サイト](http://mitsuji.org/?p=393)


### 概要

ブラウザ上で動作するシリアル通信（Bluetooth）用のターミナルツールです。  
Windows用とMac用を用意しています。  
設計上タッチセンサでのスタートを廃止したため、Bluetoothでのリモートスタートのみとなっています。  
また設定値変更などもBluetoothから行う必要があり、ターミナルから手打ちで実行するの手間を解消する目的で作ったツールです。  
（正しくはフリーソフトのIchigoLinkのサンプルUIを基に改造）


### できること

* リモートスタート
* ログの受信および表示
* echoテスト
* L/Rコース切り替え
* 輝度目標値設定（倒立走行/尻尾走行/後傾走行）


### 使用手順

1. EV3の電源をON
1. PCとEV3をペアリングする
1. Macの場合、minicomでの接続は切っておく
	* 逆にminicomでapp転送する場合はIchigoLinkは切っておく
	* 同時に同じシリアル通信のファイルディスクリプタは開けない
	* Windowsは未検証ですが、TeraTerm等と競合するかも
1. 用意してあるスクリプトでIchigoLink本体を起動
	* Windows用：start.bat（動作未検証）
	* Mac用：start.command
1. ブラウザで http://localhost:30110 へアクセス
	* ページが表示されれば成功
1. Hotcake App > Terminal のリンクをクリック
	* Hotcake Terminal のページに遷移したら成功
1. 「connect」ボタンでEV3に接続
	* ログ受信が開始されます

#### 各ボタンの説明

* connect  
EV3との通信を開始します。
* close  
EV3との通信を切断します。

##### Commands
* Echo Test  
接続確認用にエコーバックを行います。
* get Settings  
現在の全ての設定状態を確認します。
* Start  
待機状態のEV3をリモートスタートさせます。

##### Settings
* Course : L  
Lコースに設定します。
* Course : R  
Rコースに設定します。
* set（各輝度目標値）  
各輝度目標値を個別設定します。
* all set（輝度目標値）  
各輝度目標値を一括設定します。

##### Free Format
* esc  
エスケープ文字を送信します。
* send  
左のテキストボックスに入力した文字列を送信します。

##### Logs
* clear  
表示されているログをクリアします。
* copy
表示されているログ全てをクリップボードへコピーします。
* get Stored Logs
EV3本体の蓄積ログを取得表示します。



### 注意点

* Windows版は動作未確認です。
* EV3の電源OFF/ONごとに、PC側でもIchigoLink（exe/実行ファイル）を切り、使用手順1.から再接続する必要あり。
	* Hotcake Terminal（HTMLコンテンツ）は消さずにそのままでもOKです。

